3.1.4 (Brainy Betty)
e33c0acb211083648d8991fbd84a6824ecc3280c
o:Sass::Tree::RootNode
:@has_childrenT:@template"/**
 * @var {color} $loading-spinner-color
 * Background-color for the bars in the loading spinner.
 *
 * @member Ext.LoadMask
 */
$loading-spinner-color: #aaa !default;

// Private
$loading-spinner-size: 1em;
$loading-spinner-bar-width: .1em;
$loading-spinner-bar-height: .25em;

/**
 * Includes default loading spinner styles (for dataviews).
 *
 * @member Ext.LoadMask
 */
@mixin sencha-loading-spinner {
  .x-loading-spinner {
    font-size: 250%;
    height: $loading-spinner-size;
    width: $loading-spinner-size;
    position: relative;

    -webkit-transform-origin: $loading-spinner-size/2 $loading-spinner-size/2;

    /* Shared Properties for all the bars */
    & > span, & > span:before, & > span:after {
      display: block;
      position: absolute;
      width: $loading-spinner-bar-width;
      height: $loading-spinner-bar-height;
      top: 0;
      -webkit-transform-origin: $loading-spinner-bar-width/2 $loading-spinner-size/2;
      @if $include-border-radius { @include border-radius($loading-spinner-bar-width/2); }
      content: " ";
    }

    & > span {
      &.x-loading-top           {  background-color: rgba($loading-spinner-color,0.99); }
      &.x-loading-top::after    {  background-color: rgba($loading-spinner-color,0.90); }
      &.x-loading-left::before  {  background-color: rgba($loading-spinner-color,0.80); }
      &.x-loading-left          {  background-color: rgba($loading-spinner-color,0.70); }
      &.x-loading-left::after   {  background-color: rgba($loading-spinner-color,0.60); }
      &.x-loading-bottom::before{  background-color: rgba($loading-spinner-color,0.50); }
      &.x-loading-bottom        {  background-color: rgba($loading-spinner-color,0.40); }
      &.x-loading-bottom::after {  background-color: rgba($loading-spinner-color,0.35); }
      &.x-loading-right::before {  background-color: rgba($loading-spinner-color,0.30); }
      &.x-loading-right         {  background-color: rgba($loading-spinner-color,0.25); }
      &.x-loading-right::after  {  background-color: rgba($loading-spinner-color,0.20); }
      &.x-loading-top::before   {  background-color: rgba($loading-spinner-color,0.15); }
    }
  }

  .x-loading-spinner > span {
    left:         50%;
    margin-left:  -0.05em;
  }

  // .x-loading-spinner > span::before, .x-loading-spinner > span::after{  content: " "; }

  /* Rotate each of the 4 Spans */

  .x-loading-spinner > span.x-loading-top{    -webkit-transform: rotate(0deg);    -moz-transform: rotate(0deg);   }
  .x-loading-spinner > span.x-loading-right{  -webkit-transform: rotate(90deg);   -moz-transform: rotate(90deg);  }
  .x-loading-spinner > span.x-loading-bottom{ -webkit-transform: rotate(180deg);  -moz-transform: rotate(180deg); }
  .x-loading-spinner > span.x-loading-left{   -webkit-transform: rotate(270deg);  -moz-transform: rotate(270deg); }

  /* These are the two lines that surround each of the 4 Span lines */

  .x-loading-spinner > span::before{-webkit-transform: rotate(30deg);   -moz-transform: rotate(30deg);  }
  .x-loading-spinner > span::after{ -webkit-transform: rotate(-30deg);  -moz-transform: rotate(-30deg); }

  /* Set Animation */

  .x-loading-spinner {
    -webkit-animation-name: x-loading-spinner-rotate;
    -webkit-animation-duration: .5s;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-timing-function: linear;
  }

  @-webkit-keyframes x-loading-spinner-rotate{
    0%{     -webkit-transform: rotate(0deg);   }
    8.32%{  -webkit-transform: rotate(0deg);   }

    8.33%{  -webkit-transform: rotate(30deg);  }
    16.65%{ -webkit-transform: rotate(30deg);  }

    16.66%{ -webkit-transform: rotate(60deg);  }
    24.99%{ -webkit-transform: rotate(60deg);  }

    25%{    -webkit-transform: rotate(90deg);  }
    33.32%{ -webkit-transform: rotate(90deg);  }

    33.33%{ -webkit-transform: rotate(120deg); }
    41.65%{ -webkit-transform: rotate(120deg); }

    41.66%{ -webkit-transform: rotate(150deg); }
    49.99%{ -webkit-transform: rotate(150deg); }

    50%{    -webkit-transform: rotate(180deg); }
    58.32%{ -webkit-transform: rotate(180deg); }

    58.33%{ -webkit-transform: rotate(210deg); }
    66.65%{ -webkit-transform: rotate(210deg); }

    66.66%{ -webkit-transform: rotate(240deg); }
    74.99%{ -webkit-transform: rotate(240deg); }

    75%{    -webkit-transform: rotate(270deg); }
    83.32%{ -webkit-transform: rotate(270deg); }

    83.33%{ -webkit-transform: rotate(300deg); }
    91.65%{ -webkit-transform: rotate(300deg); }

    91.66%{ -webkit-transform: rotate(330deg); }
    100%{   -webkit-transform: rotate(330deg); }
  }
}:@children[o:Sass::Tree::CommentNode:
@loud0:@silent0;[ :@options{ :@value"�/**
 * @var {color} $loading-spinner-color
 * Background-color for the bars in the loading spinner.
 *
 * @member Ext.LoadMask
 */:@lines[ :
@lineio:Sass::Tree::VariableNode:
@name"loading-spinner-color:@guarded"!default:
@expro:Sass::Script::Color	:@attrs{	:redi�:
alphai:
greeni�:	bluei�;@
;0;i;[ ;@
;io;	;
0;i ;[ ;@
;"/* Private */;[ ;io;;"loading-spinner-size;0;o:Sass::Script::Number:@original"1em:@denominator_units[ ;@
:@numerator_units["em;i;i;[ ;@
;io;;"loading-spinner-bar-width;0;o;;"
0.1em;[ ;@
;["em;f0.10000000000000001 ��;i;[ ;@
;io;;"loading-spinner-bar-height;0;o;;"0.25em;[ ;@
;["em;f	0.25;i;[ ;@
;io;	;
0;0;[ ;@
;"c/**
 * Includes default loading spinner styles (for dataviews).
 *
 * @member Ext.LoadMask
 */;[ ;io:Sass::Tree::MixinDefNode:
@args[ ;"sencha-loading-spinner;T;[o:Sass::Tree::RuleNode;T:
@tabsi ;[o:Sass::Tree::PropNode;["font-size:@prop_syntax:new;!i ;[ ;@
;o:Sass::Script::String;@
:
@type:identifier;"	250%;io;";["height;#;$;!i ;[ ;@
;o:Sass::Script::Variable	:@underscored_name"loading_spinner_size;"loading-spinner-size;@
;i;io;";["
width;#;$;!i ;[ ;@
;o;(	;)"loading_spinner_size;"loading-spinner-size;@
;i;io;";["position;#;$;!i ;[ ;@
;o;%;@
;&;';"relative;io;";["-webkit-transform-origin;#;$;!i ;[ ;@
;o:Sass::Script::List	;@
:@separator:
space;[o:Sass::Script::Operation
:@operand2o;;"2;[ ;@
;[ ;i;i:@operator:div:@operand1o;(	;)"loading_spinner_size;"loading-spinner-size;@
;i;@
;io;-
;.o;;"2;@^;@
;[ ;i;i;/;0;1o;(	;)"loading_spinner_size;"loading-spinner-size;@
;i;@
;i;i;io;	;
0;0;[ ;@
;"-/* Shared Properties for all the bars */;[ ;i!o; ;T;!i ;[o;";["display;#;$;!i ;[ ;@
;o;%;@
;&;';"
block;i#o;";["position;#;$;!i ;[ ;@
;o;%;@
;&;';"absolute;i$o;";["
width;#;$;!i ;[ ;@
;o;(	;)"loading_spinner_bar_width;"loading-spinner-bar-width;@
;i%;i%o;";["height;#;$;!i ;[ ;@
;o;(	;)"loading_spinner_bar_height;"loading-spinner-bar-height;@
;i&;i&o;";["top;#;$;!i ;[ ;@
;o;%;@
;&;';"0;i'o;";["-webkit-transform-origin;#;$;!i ;[ ;@
;o;*	;@
;+;,;[o;-
;.o;;"2;@^;@
;[ ;i;i(;/;0;1o;(	;)"loading_spinner_bar_width;"loading-spinner-bar-width;@
;i(;@
;i(o;-
;.o;;"2;@^;@
;[ ;i;i(;/;0;1o;(	;)"loading_spinner_size;"loading-spinner-size;@
;i(;@
;i(;i(;i(u:Sass::Tree::IfNode�[o:Sass::Script::Variable	:@underscored_name"include_border_radius:
@name"include-border-radius:@options{ :
@linei)0[o:Sass::Tree::MixinNode:
@args[o:Sass::Script::Operation
:@operand2o:Sass::Script::Number:@original"2:@denominator_units[ ;@	:@numerator_units[ :@valuei;	i):@operator:div:@operand1o; 	;"loading_spinner_bar_width;"loading-spinner-bar-width;@	;	i);@	;	i);"border-radius:@children[ ;@	:@keywords{ ;	i)o;";["content;#;$;!i ;[ ;@
;o;%;@
;&;';"" ";i*:
@rule[".& > span, & > span:before, & > span:after;@
;i":@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i":@members[o:Sass::Selector::Sequence;7[o:#Sass::Selector::SimpleSequence;6@�;i";7[o:Sass::Selector::Parent;6@�;i"">o;9;6@�;i";7[o:Sass::Selector::Element	;["	span:@namespace0;6@�;i"o;8;7[o;9;6@�;i";7[o;:;6@�;i"">o;9;6@�;i";7[o;;	;["	span;<0;6@�;i"o:Sass::Selector::Pseudo
;["before;6@�;&:
class;i":	@arg0o;8;7[o;9;6@�;i";7[o;:;6@�;i"">o;9;6@�;i";7[o;;	;["	span;<0;6@�;i"o;=
;["
after;6@�;&;>;i";?0o; ;T;!i ;[o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o:Sass::Script::Funcall
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i.o;;"	0.99;@^;@
;[ ;f0.98999999999999999 G�;i.;"	rgba;@
:@keywords{ ;i.;i.;3["&.x-loading-top;@
;i.;4o;5;6" ;i.;7[o;8;7[o;9;6@�;i.;7[o;:;6@�;i.o:Sass::Selector::Class;["x-loading-top;6@�;i.o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i/o;;"0.9;@^;@
;[ ;f0.90000000000000002 ��;i/;"	rgba;@
;A{ ;i/;i/;3["&.x-loading-top::after;@
;i/;4o;5;6" ;i/;7[o;8;7[o;9;6@;i/;7[o;:;6@;i/o;B;["x-loading-top;6@;i/o;=
;["
after;6@;&:element;i/;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i0o;;"0.8;@^;@
;[ ;f0.80000000000000004 ��;i0;"	rgba;@
;A{ ;i0;i0;3["&.x-loading-left::before;@
;i0;4o;5;6" ;i0;7[o;8;7[o;9;6@';i0;7[o;:;6@';i0o;B;["x-loading-left;6@';i0o;=
;["before;6@';&;C;i0;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i1o;;"0.7;@^;@
;[ ;f0.69999999999999996 ff;i1;"	rgba;@
;A{ ;i1;i1;3["&.x-loading-left;@
;i1;4o;5;6" ;i1;7[o;8;7[o;9;6@H;i1;7[o;:;6@H;i1o;B;["x-loading-left;6@H;i1o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i2o;;"0.6;@^;@
;[ ;f0.59999999999999998 33;i2;"	rgba;@
;A{ ;i2;i2;3["&.x-loading-left::after;@
;i2;4o;5;6" ;i2;7[o;8;7[o;9;6@f;i2;7[o;:;6@f;i2o;B;["x-loading-left;6@f;i2o;=
;["
after;6@f;&;C;i2;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i3o;;"0.5;@^;@
;[ ;f0.5;i3;"	rgba;@
;A{ ;i3;i3;3["&.x-loading-bottom::before;@
;i3;4o;5;6" ;i3;7[o;8;7[o;9;6@�;i3;7[o;:;6@�;i3o;B;["x-loading-bottom;6@�;i3o;=
;["before;6@�;&;C;i3;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i4o;;"0.4;@^;@
;[ ;f0.40000000000000002 ��;i4;"	rgba;@
;A{ ;i4;i4;3["&.x-loading-bottom;@
;i4;4o;5;6" ;i4;7[o;8;7[o;9;6@�;i4;7[o;:;6@�;i4o;B;["x-loading-bottom;6@�;i4o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i5o;;"	0.35;@^;@
;[ ;f0.34999999999999998 ff;i5;"	rgba;@
;A{ ;i5;i5;3["&.x-loading-bottom::after;@
;i5;4o;5;6" ;i5;7[o;8;7[o;9;6@�;i5;7[o;:;6@�;i5o;B;["x-loading-bottom;6@�;i5o;=
;["
after;6@�;&;C;i5;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i6o;;"0.3;@^;@
;[ ;f0.29999999999999999 33;i6;"	rgba;@
;A{ ;i6;i6;3["&.x-loading-right::before;@
;i6;4o;5;6" ;i6;7[o;8;7[o;9;6@�;i6;7[o;:;6@�;i6o;B;["x-loading-right;6@�;i6o;=
;["before;6@�;&;C;i6;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i7o;;"	0.25;@^;@
;[ ;f	0.25;i7;"	rgba;@
;A{ ;i7;i7;3["&.x-loading-right;@
;i7;4o;5;6" ;i7;7[o;8;7[o;9;6@;i7;7[o;:;6@;i7o;B;["x-loading-right;6@;i7o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i8o;;"0.2;@^;@
;[ ;f0.20000000000000001 ��;i8;"	rgba;@
;A{ ;i8;i8;3["&.x-loading-right::after;@
;i8;4o;5;6" ;i8;7[o;8;7[o;9;6@&;i8;7[o;:;6@&;i8o;B;["x-loading-right;6@&;i8o;=
;["
after;6@&;&;C;i8;?0o; ;T;!i ;[o;";["background-color;#;$;!i ;[ ;@
;o;@
;[o;(	;)"loading_spinner_color;"loading-spinner-color;@
;i9o;;"	0.15;@^;@
;[ ;f0.14999999999999999 33;i9;"	rgba;@
;A{ ;i9;i9;3["&.x-loading-top::before;@
;i9;4o;5;6" ;i9;7[o;8;7[o;9;6@G;i9;7[o;:;6@G;i9o;B;["x-loading-top;6@G;i9o;=
;["before;6@G;&;C;i9;?0;3["& > span;@
;i-;4o;5;6" ;i-;7[o;8;7[o;9;6@W;i-;7[o;:;6@W;i-">o;9;6@W;i-;7[o;;	;["	span;<0;6@W;i-;3[".x-loading-spinner;@
;i;4o;5;6" ;i;7[o;8;7[o;9;6@g;i;7[o;B;["x-loading-spinner;6@g;io; ;T;!i ;[o;";["	left;#;$;!i ;[ ;@
;o;%;@
;&;';"50%;i>o;";["margin-left;#;$;!i ;[ ;@
;o;;"-0.05em;[ ;@
;["em;f-0.050000000000000003 ��;i?;i?;3[".x-loading-spinner > span;@
;i=;4o;5;6" ;i=;7[o;8;7[o;9;6@�;i=;7[o;B;["x-loading-spinner;6@�;i=">o;9;6@�;i=;7[o;;	;["	span;<0;6@�;i=o;	;
0;i ;[ ;@
;"`/* .x-loading-spinner > span::before, .x-loading-spinner > span::after{  content: " "; } */;[ ;iBo;	;
0;0;[ ;@
;"%/* Rotate each of the 4 Spans */;[ ;iDo; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"	0deg;[ ;@
;["deg;i ;iF;"rotate;@
;A{ ;iF;iFo;";["-moz-transform;#;$;!i ;[ ;@
;o;@
;[o;;"	0deg;[ ;@
;["deg;i ;iF;"rotate;@
;A{ ;iF;iF;3[",.x-loading-spinner > span.x-loading-top;@
;iF;4o;5;6" ;iF;7[o;8;7[o;9;6@�;iF;7[o;B;["x-loading-spinner;6@�;iF">o;9;6@�;iF;7[o;;	;["	span;<0;6@�;iFo;B;["x-loading-top;6@�;iFo; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
90deg;[ ;@
;["deg;i_;iG;"rotate;@
;A{ ;iG;iGo;";["-moz-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
90deg;[ ;@
;["deg;i_;iG;"rotate;@
;A{ ;iG;iG;3["..x-loading-spinner > span.x-loading-right;@
;iG;4o;5;6" ;iG;7[o;8;7[o;9;6@�;iG;7[o;B;["x-loading-spinner;6@�;iG">o;9;6@�;iG;7[o;;	;["	span;<0;6@�;iGo;B;["x-loading-right;6@�;iGo; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"180deg;[ ;@
;["deg;i�;iH;"rotate;@
;A{ ;iH;iHo;";["-moz-transform;#;$;!i ;[ ;@
;o;@
;[o;;"180deg;[ ;@
;["deg;i�;iH;"rotate;@
;A{ ;iH;iH;3["/.x-loading-spinner > span.x-loading-bottom;@
;iH;4o;5;6" ;iH;7[o;8;7[o;9;6@;iH;7[o;B;["x-loading-spinner;6@;iH">o;9;6@;iH;7[o;;	;["	span;<0;6@;iHo;B;["x-loading-bottom;6@;iHo; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"270deg;[ ;@
;["deg;i;iI;"rotate;@
;A{ ;iI;iIo;";["-moz-transform;#;$;!i ;[ ;@
;o;@
;[o;;"270deg;[ ;@
;["deg;i;iI;"rotate;@
;A{ ;iI;iI;3["-.x-loading-spinner > span.x-loading-left;@
;iI;4o;5;6" ;iI;7[o;8;7[o;9;6@N;iI;7[o;B;["x-loading-spinner;6@N;iI">o;9;6@N;iI;7[o;;	;["	span;<0;6@N;iIo;B;["x-loading-left;6@N;iIo;	;
0;0;[ ;@
;"I/* These are the two lines that surround each of the 4 Span lines */;[ ;iKo; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
30deg;[ ;@
;["deg;i#;iM;"rotate;@
;A{ ;iM;iMo;";["-moz-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
30deg;[ ;@
;["deg;i#;iM;"rotate;@
;A{ ;iM;iM;3["&.x-loading-spinner > span::before;@
;iM;4o;5;6" ;iM;7[o;8;7[o;9;6@�;iM;7[o;B;["x-loading-spinner;6@�;iM">o;9;6@�;iM;7[o;;	;["	span;<0;6@�;iMo;=
;["before;6@�;&;C;iM;?0o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"-30deg;[ ;@
;["deg;i�;iN;"rotate;@
;A{ ;iN;iNo;";["-moz-transform;#;$;!i ;[ ;@
;o;@
;[o;;"-30deg;[ ;@
;["deg;i�;iN;"rotate;@
;A{ ;iN;iN;3["%.x-loading-spinner > span::after;@
;iN;4o;5;6" ;iN;7[o;8;7[o;9;6@�;iN;7[o;B;["x-loading-spinner;6@�;iN">o;9;6@�;iN;7[o;;	;["	span;<0;6@�;iNo;=
;["
after;6@�;&;C;iN;?0o;	;
0;0;[ ;@
;"/* Set Animation */;[ ;iPo; ;T;!i ;[	o;";["-webkit-animation-name;#;$;!i ;[ ;@
;o;%;@
;&;';"x-loading-spinner-rotate;iSo;";["-webkit-animation-duration;#;$;!i ;[ ;@
;o;%;@
;&;';".5s;iTo;";["&-webkit-animation-iteration-count;#;$;!i ;[ ;@
;o;%;@
;&;';"infinite;iUo;";["&-webkit-animation-timing-function;#;$;!i ;[ ;@
;o;%;@
;&;';"linear;iV;3[".x-loading-spinner;@
;iR;4o;5;6" ;iR;7[o;8;7[o;9;6@�;iR;7[o;B;["x-loading-spinner;6@�;iRo:Sass::Tree::DirectiveNode
;T;[o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"	0deg;[ ;@
;["deg;i ;iZ;"rotate;@
;A{ ;iZ;iZ;3["0%;@
;iZ;4o;5;6" ;iZ;7[o;8;7[["0%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"	0deg;[ ;@
;["deg;i ;i[;"rotate;@
;A{ ;i[;i[;3["
8.32%;@
;i[;4o;5;6" ;i[;7[o;8;7[[
"8" "."32%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
30deg;[ ;@
;["deg;i#;i];"rotate;@
;A{ ;i];i];3["
8.33%;@
;i];4o;5;6" ;i];7[o;8;7[[
"8" "."33%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
30deg;[ ;@
;["deg;i#;i^;"rotate;@
;A{ ;i^;i^;3["16.65%;@
;i^;4o;5;6" ;i^;7[o;8;7[[
"16" "."65%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
60deg;[ ;@
;["deg;iA;i`;"rotate;@
;A{ ;i`;i`;3["16.66%;@
;i`;4o;5;6" ;i`;7[o;8;7[[
"16" "."66%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
60deg;[ ;@
;["deg;iA;ia;"rotate;@
;A{ ;ia;ia;3["24.99%;@
;ia;4o;5;6" ;ia;7[o;8;7[[
"24" "."99%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
90deg;[ ;@
;["deg;i_;ic;"rotate;@
;A{ ;ic;ic;3["25%;@
;ic;4o;5;6" ;ic;7[o;8;7[["25%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"
90deg;[ ;@
;["deg;i_;id;"rotate;@
;A{ ;id;id;3["33.32%;@
;id;4o;5;6" ;id;7[o;8;7[[
"33" "."32%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"120deg;[ ;@
;["deg;i};if;"rotate;@
;A{ ;if;if;3["33.33%;@
;if;4o;5;6" ;if;7[o;8;7[[
"33" "."33%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"120deg;[ ;@
;["deg;i};ig;"rotate;@
;A{ ;ig;ig;3["41.65%;@
;ig;4o;5;6" ;ig;7[o;8;7[[
"41" "."65%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"150deg;[ ;@
;["deg;i�;ii;"rotate;@
;A{ ;ii;ii;3["41.66%;@
;ii;4o;5;6" ;ii;7[o;8;7[[
"41" "."66%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"150deg;[ ;@
;["deg;i�;ij;"rotate;@
;A{ ;ij;ij;3["49.99%;@
;ij;4o;5;6" ;ij;7[o;8;7[[
"49" "."99%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"180deg;[ ;@
;["deg;i�;il;"rotate;@
;A{ ;il;il;3["50%;@
;il;4o;5;6" ;il;7[o;8;7[["50%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"180deg;[ ;@
;["deg;i�;im;"rotate;@
;A{ ;im;im;3["58.32%;@
;im;4o;5;6" ;im;7[o;8;7[[
"58" "."32%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"210deg;[ ;@
;["deg;i�;io;"rotate;@
;A{ ;io;io;3["58.33%;@
;io;4o;5;6" ;io;7[o;8;7[[
"58" "."33%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"210deg;[ ;@
;["deg;i�;ip;"rotate;@
;A{ ;ip;ip;3["66.65%;@
;ip;4o;5;6" ;ip;7[o;8;7[[
"66" "."65%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"240deg;[ ;@
;["deg;i�;ir;"rotate;@
;A{ ;ir;ir;3["66.66%;@
;ir;4o;5;6" ;ir;7[o;8;7[[
"66" "."66%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"240deg;[ ;@
;["deg;i�;is;"rotate;@
;A{ ;is;is;3["74.99%;@
;is;4o;5;6" ;is;7[o;8;7[[
"74" "."99%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"270deg;[ ;@
;["deg;i;iu;"rotate;@
;A{ ;iu;iu;3["75%;@
;iu;4o;5;6" ;iu;7[o;8;7[["75%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"270deg;[ ;@
;["deg;i;iv;"rotate;@
;A{ ;iv;iv;3["83.32%;@
;iv;4o;5;6" ;iv;7[o;8;7[[
"83" "."32%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"300deg;[ ;@
;["deg;i,;ix;"rotate;@
;A{ ;ix;ix;3["83.33%;@
;ix;4o;5;6" ;ix;7[o;8;7[[
"83" "."33%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"300deg;[ ;@
;["deg;i,;iy;"rotate;@
;A{ ;iy;iy;3["91.65%;@
;iy;4o;5;6" ;iy;7[o;8;7[[
"91" "."65%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"330deg;[ ;@
;["deg;iJ;i{;"rotate;@
;A{ ;i{;i{;3["91.66%;@
;i{;4o;5;6" ;i{;7[o;8;7[[
"91" "."66%" o; ;T;!i ;[o;";["-webkit-transform;#;$;!i ;[ ;@
;o;@
;[o;;"330deg;[ ;@
;["deg;iJ;i|;"rotate;@
;A{ ;i|;i|;3["	100%;@
;i|;4o;5;6" ;i|;7[o;8;7[["	100%" ;@
;"0@-webkit-keyframes x-loading-spinner-rotate;iY;@
;i;@
;i