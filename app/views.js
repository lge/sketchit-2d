sketchit.views.Bottombar = Ext.extend(Ext.Toolbar, {
	initComponent : function() {
		Ext.apply(this, {
			dock : "bottom",
			ui : "light",
			defaults : {
				iconMask : true,
				iconAlign : 'top',
			},
			scroll : {
				direction : 'horizontal',
				useIndicators : false
			},
			items : [{
				text : 'setting',
				width : 45,
				iconCls : 'settings3',
				style : {
					"font-size" : "80%"
				},
				id : 'settingButton'
			}, {
				width : 45,
				text : 'run',
				iconCls : 'run',
				style : {
					"font-size" : "80%"
				},
				id : 'runButton'
			}, {
				text : 'rescale',
				width : 45,
				iconCls : 'contract',
				style : {
					"font-size" : "80%"
				},
				id : 'rescaleButton'
			}, {
				text : 'mesh',
				iconCls : 'mesh',
				width : 45,
				style : {
					"font-size" : "80%"
				},
				id : 'meshButton'
			}, {
				xtype : 'spacer',
			}, {
				ui : "plain",
				iconCls : 'refresh5',
				id : 'clearAllButton'
			}, {
				ui : "plain",
				iconCls : 'unselect',
				id : 'unselectAllButton'
			}, {
				iconCls : 'trash',
				ui : "plain",
				id : 'deleteButton'
			}, {
				ui : "plain",
				iconCls : 'undo',
				id : 'undoButton'
			}, {
				ui : "plain",
				iconCls : 'redo',
				id : 'redoButton'
			}, {
				ui : "plain",
				iconCls : 'save',
				id : 'saveButton'
			}, {
				ui : "plain",
				iconCls : 'monitor1',
				id : 'terminalButton'
			},{
				ui : "plain",
				iconCls : 'open',
				id : 'importButton'
			}, {
				iconCls : 'log',
				ui : "plain",
				id : 'logButton'
			}, {
				xtype : 'spacer',
			}, {
				xtype : 'segmentedbutton',
				id : 'modeSelector',
				defaults : {
					iconMask : true,
					iconAlign : 'top',
					width : 45,
					style : {
						"font-size" : "80%"
					}
				},
				items : [{
					text : 'draw',
					iconCls : 'draw',
					pressed : true,
				}, {
					text : 'select',
					iconCls : 'select',
				},
                //          {
				// 	text : 'load',
				// 	iconCls : 'load',
				// },
                         {
					text : 'move',
					iconCls : 'move',
				}]
			}]
		});

		sketchit.views.Bottombar.superclass.initComponent.apply(this, arguments);
	}
});
Ext.reg('sketchitBottombar', sketchit.views.Bottombar);

sketchit.views.Topbar = Ext.extend(Ext.Toolbar, {
	initComponent : function() {
		Ext.apply(this, {
			dock : "top",
			layout : "hbox",
			ui : "light",
			defaults : {
				iconMask : true,
				ui : "plain",
			},
			scroll : {
				direction : 'horizontal',
				useIndicators : false
			},
			items : [{
				xtype : 'segmentedbutton',
				items : [{
					text : 'Static',
					pressed : true,
					width : 80
				}, {
					text : 'Dynamic',
					width : 80
				}, {
					text : 'Eigen',
					width : 80
				}]
			},{
				xtype: 'spacer'
			},{
				xtype: 'button',
				iconCls : 'look',
				// disabled : true
			},{
				xtype: 'button',
				iconCls : 'play_black2',
				// disabled : true
			},{
				xtype: 'button',
				iconCls : 'cube',
				// disabled : true
			}]
		});
		sketchit.views.Topbar.superclass.initComponent.apply(this, arguments);
	}
});
Ext.reg('sketchitTopbar', sketchit.views.Topbar);


sketchit.views.Canvas = Ext.extend(Ext.Panel, {

	layout : 'fit',
	Renderer : undefined,

	style : {
		"background-color" : "white"
	},

});
Ext.reg('sketchitCanvas', sketchit.views.Canvas);

sketchit.views.AnimateWidget = Ext.extend(Ext.Toolbar,{
	floating: true,
	draggable: true,
	centered: true,
	// width : 200,
	hight : 100,
	hideOnMaskTap : false,
	layout: {
		// type : "hbox"
	},
	defaults : {
		xtype : 'button',
		iconMask : true
		// iconAlign : 'top',
	},
	items : [{
		// text : 'play',
		iconCls: 'ButtonFFW'
		
	},{
		iconCls : 'ButtonStop'
	},{
		iconCls : 'ButtonRemove'
	},{
		iconCls : 'ButtonRemove'
	}]
	
	
})
Ext.reg('sketchitAnimateWidget', sketchit.views.AnimateWidget);

var myTabBarTop = {
	ui: 'light',
	dock: 'top',
	baseCls: 'x-tabbar',
	layout: {
		pack: 'center',
		type: 'hbox'
	}
};

sketchit.views.ExportModelPanel = Ext.extend(Ext.TabPanel,{
	floating: true,
	draggable: true,
	centered: true,
	width : 400,
	height : 400,
	hidden : true,
	tabBar : myTabBarTop,
	defaults : {
		xtype : 'textareafield',
		maxRows : 12
	},
	items : [{
		title : 'Tcl',
	},{
		title : 'JSON',
	}]
	
	
})

var displaySettingPanel = {
	title: 'display',
	xtype:'panel',
	ui:'light',
    scroll : 'vertical',
    defaults : {
    	xtype : 'togglefield',
    	labelWidth : '80%',
    	flex : 1
    },
	items : [{
        id : 'toggle_structure',
        label : 'structure'
    },{
        id : 'toggle_deformation',
    	label : 'deformation'
    },{
        id : 'toggle_moment',
    	label : 'moment'
    },{
        id : 'toggle_node',
    	label : 'node'
    },{
        id : 'toggle_node_tag',
    	label : 'node tag'
    },{
        id : 'toggle_element_tag',
    	label : 'element tag'
    },{
        id : 'toggle_element_direction',
    	label : 'element direction'
    },{
        id : 'toggle_loads',
    	label : 'loads'
    },{
        id : 'toggle_constraints',
    	label : 'constraints'
    },{
        id : 'toggle_bgImage',
    	label : 'background image'
    }]
};

var modelSettingPanel = {
	title: 'model',
	ui:'light',
    scroll : 'vertical',
    defaults : {
    	xtype: 'spinnerfield',
    	labelWidth : '50%',
    	flex : 1,
    },
    items : [{
    	label : 'number of dimension',
    	maxValue : 3,
    	minValue : 1,
    	value : 2
    },{
    	label : 'number of DOF',
    	maxValue : 6,
    	minValue : 1,
    	value : 3
    },{
    	xtype : 'togglefield',
    	label : 'enable auto mesh',
    	labelWidth : '80%',
    },{
    	label : 'mesh size',
    	value : 20,
    	maxValue : 100,
    	minValue : 5,
    }]
};

var analysisSettingPanel = {
	title: 'analsis',
	ui:'light',
    scroll : 'vertical',
    defaults : {
    	xtype : 'selectfield',
    	labelWidth : '50%',
    	flex : 1,
    },
    items : [{
    	label : 'analysis type',
    	options :[
	        {text: 'static',  value: 'static'},
	        {text: 'dynamic', value: 'dynamic'}
	    ]
    },{
    	xtype : 'togglefield',
    	label : 'enable auto run',
    },{
    	label : 'time series',
    	options :[
	        {text: 'constant',  value: 'constant'},
	        {text: 'linear', value: 'linear'},
	        {text: 'input ground motion', value: 'path'}
	    ]
    },{
    	label : 'algorithm',
    	options :[
    		{text: 'Newton', value: 'Newton'},
	        {text: 'Newton line search',  value: 'NewtonLineSearch'},
	        {text: 'Modified Newton',  value: 'ModifiedNewton'},
	    ]
    },{
    	label : 'test',
    	options :[
	        {text: 'unbalanced load',  value: 'unbalancedload'},
	        {text: 'incremental displacement', value: 'incrdisp'},
	    ]
    },{
    	label : 'integrator',
    	options :[
	        {text: 'load control',  value: 'loadcontrol'},
	    ]
    },{
    	label : 'constaint',
    	options :[
	        {text: 'plain',  value: 'plain'},
	        {text: 'lagrange', value: 'lagrange'},
	    ]
    },{
    	label : 'numberer',
    	options :[
	        {text: 'plain',  value: 'plain'},
	        {text: 'RCM',  value: 'RCM'}
	    ]
    },{
    	label : 'system',
    	options :[
	        {text: 'Band General',  value: 'Bandgeneral'},
	    ]
    }]
};

var elementSettingPanel = {
	
	title: 'elements',
    defaults : {
    	labelWidth : '50%',
    },
    items : [{
	    	xtype: 'selectfield',
	    	label : 'default element type',
	    	options : [
	    		{text: 'elastic beam', value: 'elasticBeamColumn'},
	    		{text: 'truss', value: 'truss'},
	    		{text: 'non-linear beam', value: 'nonlinearBeamColumn'},
	    		{text: 'beam with plastic hinges', value: 'beamWithHinges'}
	    	],
	    	listeners : {
	    		change : function(obj) {
	    			alert('select changed!!'+obj.getValue());
	    		}
	    		
	    	}
    	},{
        	label : 'A',
        	xtype : 'spinnerfield',
        	value:  100
        },{
        	label : 'E',
        	xtype : 'spinnerfield',
        	value : 29000
        },{
        	label : 'I',
        	xtype : 'spinnerfield',
        	value:  833.3333
    	},{
    		label : 'sectionID',
        	xtype : 'textfield',
        	value:  1
    	},{
    		label : 'geomtranformID',
        	xtype : 'textfield',
        	value:  3
    	}
	]
};

var advanceSettingPanel = {
	title: 'advanced',
	layout: {
		type: "auto"
	},
    items : [{
    	xtype : 'textareafield',
        maxRows : 12,
    },{
    	xtype : 'button',
    	ui: 'confirm',
    	text : 'apply',
    }]
};


sketchit.views.SettingTabs = Ext.extend(Ext.TabPanel,{
	floating: true,
	hideOnMaskTap : false,
	hidden : true,
	width: 500,
	height: 400,
	style : {
		opacity:0.8
	},
	tabBar : myTabBarTop,
	items: [
		displaySettingPanel,
		modelSettingPanel,
		analysisSettingPanel,
		elementSettingPanel,
		advanceSettingPanel
	],	
});

var allDomainComponents = {
	title: 'all',
	layout: {
		type: "auto"
	},
    items : [{
    	xtype : 'textareafield',
        maxRows : 12,
    },{
    	xtype : 'button',
    	ui: 'confirm',
    	text : 'apply',
    }]
}

var selectedDomainComponents = {
	title: 'selected',
	layout: {
		type: "auto"
	},
    items : [{
    	xtype : 'textareafield',
        maxRows : 12,
    },{
    	xtype : 'button',
    	ui: 'confirm',
    	text : 'apply',
    }]
}

Ext.regModel('eleListItem', {
    fields: ['id', 'className']
});
var testData = [
        // {firstName: 'Tommy',   lastName: 'Maintz'},
        // {firstName: 'Rob',     lastName: 'Dougan'},
        // {firstName: 'Ed',      lastName: 'Spencer'},
        // {firstName: 'Jamie',   lastName: 'Avins'},
        // {firstName: 'Aaron',   lastName: 'Conran'},
        // {firstName: 'Dave',    lastName: 'Kaneda'},
        // {firstName: 'Michael', lastName: 'Mullany'},
        // {firstName: 'Abraham', lastName: 'Elias'},
        // {firstName: 'Jay',     lastName: 'Robinson'}
];

var store = new Ext.data.JsonStore({
    model  : 'eleListItem',
    sorters: 'id',

    // getGroupString : function(record) {
        // return record.get('lastName')[0];
    // },
    data:testData

    // data: [
        // {firstName: 'Tommy',   lastName: 'Maintz'},
        // {firstName: 'Rob',     lastName: 'Dougan'},
        // {firstName: 'Ed',      lastName: 'Spencer'},
        // {firstName: 'Jamie',   lastName: 'Avins'},
        // {firstName: 'Aaron',   lastName: 'Conran'},
        // {firstName: 'Dave',    lastName: 'Kaneda'},
        // {firstName: 'Michael', lastName: 'Mullany'},
        // {firstName: 'Abraham', lastName: 'Elias'},
        // {firstName: 'Jay',     lastName: 'Robinson'}
    // ]
});


sketchit.views.elementList = new Ext.List({
    floating: true,
    hidden : true,
    centered: true,
    width : 450,
    height: 500,
    itemTpl : '{id}{className}',
    // grouped : true,
    // indexBar: true,
    
    store: store
});

var getPropertyFromArray=function(arr,prop) {
	var val;
	var el;
	
	for (var i=0; i < arr.length; i++) {
		el = arr[i];
		if($D.isDefined(val)) {
			if (el[prop]!=val) {
				val = "*";
				break;
			}
		} else {
			val = el[prop];
		}
	};
	return val;
}

var setPropertyForArray=function(arr,prop,val) {
	for (var i=0; i < arr.length; i++) {
		arr[i][prop]=val;
	};
	return arr.length;
}


var elementView = Ext.extend(Ext.Panel,{
	
	title : "elements",
	initComponent : function(){
		elementView.superclass.initComponent.apply(this, arguments);
		this.getComponent(1).on('change',function(obj){
			this.updateElementAttrFields(obj.getValue());
		},this);
		this.getDockedComponent(0).setHandler(function(){
			this.writeData(sketchit.Domain.theElements);
		},this)
		
		this.updateElementAttrFields("ElasticBeamColumn");
		// var arr = sketchit.dataAdapter.getElementList(function(el){
			// return el.isSelected;
		// });
		// this.readData(arr);
		
		
		
		
		
	},
	scroll: 'vertical',
	items : [{
			xtype: "textfield",
			label : "number of selected elements",
			disabled : true
		},{
	    	xtype: 'selectfield',
	    	label : 'element type',
	    	options : [
	    		{text: 'ElasticBeamColumn', value: 'ElasticBeamColumn'},
	    		{text: 'Truss', value: 'Truss'},
	    		{text: 'NonlinearBeamColumn', value: 'NonlinearBeamColumn'},
	    		{text: 'BeamWithHinges', value: 'BeamWithHinges'},
	    		// {text: '*', value: '*'}
	    	],

    	},
    	
	],
	dockedItems : [{
			dock : 'bottom',
			xtype : 'button',
			ui : 'confirm',
			text : 'apply',
			// handler : function(b,e) {
				// this.writeData(sketchit.Domain);
// 				
			// },
			// writeData : function(dm) {
				// alert('write data to domain!')
				// console.log('domain:',dm)
// 				
// 				
			// },
		},{
			dock : 'bottom',
			xtype : 'button',
			ui : 'cancel',
			text : 'cancel'
		}
	],
	writeData : function(target) {
		var ids = this.selectedEID;
		alert('write data to target!')
		console.log('target:',target);
		var len = this.items.length;
		var cmp;
		for(var i = 2;i<len;i++) {
			cmp = this.getComponent(i);
			
			for (var j=0; j < ids.length; j++) {
				target[ids[j]][cmp.propertyKey]=cmp.getValue();
			};
			// setPropertyForArray(dm.theElements,cmp.propertyKey,cmp.getValue());
		}
		
		
		
	},
	updateElementAttrFields : function(type) {
		// var type = this.getComponent(0).getValue();
		console.log('type=',type);
		var optionCopy=this.getComponent(0);
		var len = this.items.length;
		for(var i = 2;i<len;i++) {
			this.remove(this.getComponent(2));
		}
		// this.removeAll();
		// this.add(optionCopy);
		var attrs=[];
		if(type==='ElasticBeamColumn') {
			attrs=[{
	        	label : 'A',
	        	xtype : 'spinnerfield',
	        	propertyKey : 'A'
	        	// value:  100
	        },{
	        	label : 'E',
	        	xtype : 'spinnerfield',
	        	propertyKey : 'E'
	        	// value : 29000
	        },{
	        	label : 'I',
	        	xtype : 'spinnerfield',
	        	propertyKey : 'I'
	        	// value:  833.3333
	    	},{
	    		label : 'geomtranformID',
	        	xtype : 'textfield',
	        	propertyKey : 'geomtranformID'
	        	// value:  3
	    	}];
			
		} else if(type==='Truss') {
			attrs=[{
	        	label : 'A',
	        	xtype : 'spinnerfield',
	        },{
	        	label : 'E',
	        	xtype : 'spinnerfield',
	        	// value : 29000
	        },{
	    		label : 'geomtranformID',
	        	xtype : 'textfield',
	        	// value:  3
	    	}];
		} else if(type==='NonlinearBeamColumn') {
			attrs=[{
	        	label : 'number of integration points',
	        	xtype : 'spinnerfield',
	        	// value:  100
	        },{
	    		label : 'sectionScripts',
	        	xtype : 'textareafield',
	        	// value:  1
	    	},{
	    		label : 'geomtranformID',
	        	xtype : 'textfield',
	        	// value:  3
	    	}];
		} else if(type==='BeamWithHinges') {
			attrs=[{
	        	label : 'lpi',
	        	xtype : 'spinnerfield',
	        	// value:  100
	        },{
	    		label : 'lpj',
	        	xtype : 'textfield',
	        	// value:  1
	    	},{
	    		label : 'geomtranformID',
	        	xtype : 'textfield',
	        	// value:  3
	    	}];
		} else if(type==='*') {
			
		}
		this.add(attrs);
		this.doLayout();
		
	},
	
	
	readData : function(elementList) {
		var type;
		var el;
		var count=elementList.length;
		this.selectedEID=[];
		
		for (var i=0; i < elementList.length; i++) {
			this.selectedEID.push(elementList[i].id);
		};
 		
 		type = getPropertyFromArray(elementList,"className");
		this.getComponent(0).setValue(count);
		this.getComponent(1).setValue(type);
		this.updateElementAttrFields(type);
		
		// read the value from each field:
		if(type==='ElasticBeamColumn') {
			// this.readElasticBeamColumnData();
			var A;
			var E;
			var I;
			var geomTransfId;
			A = getPropertyFromArray(elementList,"A");
			E = getPropertyFromArray(elementList,"E");
			I = getPropertyFromArray(elementList,"I");
			geomTransfId = getPropertyFromArray(elementList,"geomTransfId");
			this.getComponent(2).setValue(A);
			this.getComponent(3).setValue(E);
			this.getComponent(4).setValue(I);
			this.getComponent(5).setValue(geomTransfId);
			
			
		} else if(type==='Truss') {
			attrs=[{
	        	label : 'A',
	        	xtype : 'spinnerfield',
	        },{
	        	label : 'E',
	        	xtype : 'spinnerfield',
	        }];
		} else if(type==='NonlinearBeamColumn') {
			attrs=[{
	        	label : 'number of integration points',
	        	xtype : 'spinnerfield',
	        },{
	    		label : 'sectionScripts',
	        	xtype : 'textareafield',
	    	},{
	    		label : 'geomtranformID',
	        	xtype : 'textfield',
	    	}];
		} else if(type==='BeamWithHinges') {
			attrs=[{
	        	label : 'lpi',
	        	xtype : 'spinnerfield',
	        },{
	    		label : 'lpj',
	        	xtype : 'textfield',
	    	},{
	    		label : 'geomtranformID',
	        	xtype : 'textfield',
	    	}];
		} else if(type==='*') {
			
		}
		
		
	},
	
	
	
	
})

var nodeView = Ext.extend(Ext.Panel,{
	
	floating : true,
	hidden : true,
	title : "nodes",
	text : "nodes!!!"
	
	
	
})

// list.show();

// var elementList = Ext.extend(Ext.List,{
// 	
	// loadData:function(eleStore){
// 		
	// },
// 	
// 	
// 	
// })

sketchit.views.Inspector = Ext.extend(Ext.TabPanel,{
	floating: true,
	hideOnMaskTap : false,
	// centered : t
	hidden : true,
	width: 500,
	height: 400,
	style : {
		opacity:0.8
	},
	tabBar : myTabBarTop,
	items: [
		new elementView(),
		nodeView
	],	
});

var sectionformView = {
	title: 'formView',
	layout: {
		type: "auto"
	},
    items : [{
    	xtype : 'textfield',
    	label: 'tag',
    },{
    	xtype : 'textfield',
    	label: 'type',
    },{
    	xtype : 'button',
    	ui: 'confirm',
    	text : 'OK',
    }]
	
}

var sectionScripts = {
	title: 'script',
	layout: {
		type: "auto"
	},
    items : [{
    	xtype : 'textareafield',
        maxRows : 12,
    },{
    	xtype : 'button',
    	ui: 'confirm',
    	text : 'import',
    }]
	
}

sketchit.views.sectionViewer = Ext.extend(Ext.TabPanel,{
	floating: true,
	hideOnMaskTap : false,
	// centered : t
	hidden : true,
	width: 500,
	height: 400,
	style : {
		opacity:0.8
	},
	tabBar : myTabBarTop,
	items: [
		sectionformView,
		sectionScripts
	],	
});

var loadSettings = function() {
	var str = "";
	var S = this.settings;
	var value;
	for (var key in S) {
		value = S[key];
		if ($D.isString(value)) {
			value = "\""+value+"\"";
		}
		str += key+" : "+ value+ ",\n";
	}
	str = str.slice(0,-2);				
	settingTabs.getComponent(0).getComponent(4).getComponent(0).setValue(str)
};

var applySettings = function() {
	eval("temp={"+settingTabs.getComponent(0).getComponent(4).getComponent(0).getValue()+"}")
	$D.apply(this.settings,temp);
	console.log("settings",temp)
}
			

sketchit.views.Viewport = Ext.extend(Ext.Panel, {

	layout : 'fit',
	fullscreen : true,

	items : [{
		xtype : 'sketchitCanvas',
		id : 'canvas',
		html : '<canvas id="workspace"  width="' + 1024 + '" height="' + 768 + '">no canvas support</canvas>',
	}],

	dockedItems : [{
		xtype : 'sketchitTopbar',
		id : 'topBar'
	}, {
		xtype : 'sketchitBottombar',
		id : 'bottomBar'
	}],
});
