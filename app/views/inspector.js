var elementSettingPanel = {
	
	title: 'elements',
    defaults : {
    	labelWidth : '50%',
    },
    items : [{
    	xtype: 'selectfield',
    	label : 'default element type',
    	options : [
    		{text: 'elastic beam', value: 'elasticBeamColumn'},
    		{text: 'truss', value: 'truss'},
    		{text: 'non-linear beam', value: 'nonlinearBeamColumn'},
    		{text: 'beam with plastic hinges', value: 'beamWithHinges'}
    	]},{
        	label : 'A',
        	xtype : 'spinnerfield',
        	value:  100
        },{
        	label : 'E',
        	xtype : 'spinnerfield',
        	value : 29000
        },{
        	label : 'I',
        	xtype : 'spinnerfield',
        	value:  833.3333
    	},{
    		label : 'sectionID',
        	xtype : 'textfield',
        	value:  1
    	},{
    		label : 'geomtranformID',
        	xtype : 'textfield',
        	value:  3
    	}
	]
};
var myTabBarTop = {
	ui: 'light',
	dock: 'top',
	baseCls: 'x-tabbar',
	layout: {
		pack: 'center',
		type: 'hbox'
	}
};


sketchit.views.elementInspector = Ext.extend(Ext.TabPanel,{
	floating: true,
	hideOnMaskTap : false,
	// centered : t
	hidden : true,
	width: 500,
	height: 400,
	style : {
		opacity:0.8
	},
	tabBar : myTabBarTop,
	items: [
		allDomainComponents,
		selectedDomainComponents
	],	
});