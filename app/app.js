(function() {
	var $D = DirectFEA;
	var fps = 0, now, lastUpdate = (new Date)*1 - 1;
	// The higher this value, the less the FPS will be affected by quick changes
	// Setting this to 1 will show you the FPS of the last sampled frame only
	var fpsFilter = 50;
	var isAnalysisReadyToRun = false;
	var duplicateNodes = false;
	var defaultSettings = {
		mode : 'draw',
		fps : 100,
		touchMoveAnimation : false,
		touchMoveFps : 10,
		snapAnimationFps : 50,
		snapAnimationElapse : 1500,
		dynamicsAnalysis: false,
		dynamicAnalysisNsteps : 700,
		analysisType : 'Static',

		autoAnalysis : true,
		showMessage : true,
		messageBoxPositionX : 10,
		messageBoxPositionY : 10,
		messageTextFont : "bold 12px sans-serif",
		messageTextFillStye : "black",
		messageLastTime : 5,
		
		frameRatePositionX : 10,
		frameRatePositionY : 30,

		modelScale : 2.0,
		loadScale : 100.0,
		// loadScale : 1.0,

		mouseWheelSpeedFactor : 5, //1~10, scale = e^(wheelDelta*speedFactor/speedBase)
		mouseWheelSpeedBase : 5000, //fixed

		viewPortScale : 1.0,
		maxViewPortScale : 20.0,
		minViewPortScale : 0.2,
		viewPortShiftX : 0.0,
		viewPortShiftY : 0.0,

		showDeformation : true,
		showMoment : true,

		// deformationScale : 20,
		deformationScale : 300,
		momentScale : 0.01,
		autoDeformationScale : true,
		maxDeformationOnScreen : 40,
		deformationResolution : 20,
		autoMomentScale : true,
		maxMomentOnScreen : 80,
		momentResolution : 20,

		snapToNode : true,
		snapToNodeThreshold : 10,
		moveSnapToNodeThreshold: 10,

		snapToLine : true,
		snapToLineThreshold : 25,

		snapToGrid : true,
		grid : 20,
		gridLineWidth : 1,
		gridLineStyle : "rgba(0,0,0,0.5)",

		SPCSnapToDirection : true,
		SPCSnapToDirectionThreshold : 0.2,

		SPCSnapToNode : true,
		SPCSnapToNodeThreshold : 15,

		SPCSnapToLine : true,
		SPCSnapToLineThreshold : 15,

		circleSnapToSPCThreshold : 25,

		loadSnapToNode: true,
		loadSnapToNodeThreshold : 15,
		
		loadSnapToLine : true,
		loadSnapToLineThreshold : 15,

		autoMergeNodeOnLine : true,
		autoMergeNodeOnLineThreshold : 10,

		showStructure: true,
		showNodeId : false,
		showElementId : false,
		showMarks : false,
		showGrid : true,
		showSPC : true,
		showLineElementDirection : false,
		showConstraints: true,
		showLoads:true,
		showNodes: true,
		showBgImage: true,
		

		canvasBgColor : "rgb(255,255,255)",
		inputStrokeStyle : "rgb(0,0,255)",
		inputStrokeWidth : 2,

		// defaultLineELementType : DirectFEA.ElasticBeamColumn,
		defaultLineELementType : "ElasticBeamColumn",
		defaultLineELementA : 44.8,
		defaultLineELementI : 8160,
        
		defaultElasticBeamA : 44.8,
		defaultElasticBeamI : 8160,
		defaultElasticBeamE : 29000,
		defaultElasticBeamDensity : 0.284 / 1000,

        
		// defaultGeomTransf : Domain.theGeomTransfs[3], 
		defaultGeomTransfId : 3,// 1:linear, 2:PDelta, 3:Corotational
		defaultNodeLoadType : "load",

		UniformElementLoadDirectionThreshold : 0.3,
		UniformElementLoadSnapToLineThreshold : 15,

		circleSelectThreshold : 0.1,
		clickSelectThreshold : 10,
		
		autoMesh: true,
		autoMeshSize: 10
	};
	
	
	// sketchit = Ext.application({
	sketchit = new Ext.Application({
  		name: "sketchit",
	
	    launch: function() {
	        this.views.viewport = new this.views.Viewport();
	        
	        this.views.exportModelPanel = new this.views.ExportModelPanel();
	        
	        this.views.settingTabs = new this.views.SettingTabs();
	        this.views.settingTabs.getComponent(4).on("show",this.loadSettingsToSettingTabs,this);
			this.views.settingTabs.getComponent(4).getComponent(1).setHandler(this.applySettingsFromSettingTabs,this);
			this.views.inspector = new sketchit.views.Inspector();
			this.views.elementView = this.views.inspector.getComponent(0);
			this.views.nodeView = this.views.inspector.getComponent(1);
			
			this.views.animator = new sketchit.views.AnimateWidget({
				hidden : true
			});
	        
	
	        // var canvas = this.views.viewport.getComponent(0);
	        var canvas = this.views.viewport.down("#canvas");
	        
			var topBar = this.views.viewport.down("#topBar");;
			var bottomBar = this.views.viewport.down("#bottomBar");;
	        this.views.canvas = canvas;
			this.views.topBar = topBar;
			this.views.bottomBar = bottomBar;
			
			this.views.settingButton = bottomBar.getComponent(0);
			this.views.runButton = bottomBar.getComponent(1);
			this.views.rescaleButton = bottomBar.getComponent(2);
			this.views.meshButton = bottomBar.getComponent(3);
			this.views.clearAllButton = bottomBar.getComponent(5);
			this.views.unselectAllButton = bottomBar.getComponent(6);
			this.views.deleteButton = bottomBar.getComponent(7);
			this.views.undoButton = bottomBar.getComponent(8);
			this.views.redoButton = bottomBar.getComponent(9);
			this.views.saveButton = bottomBar.getComponent(10);
			this.views.terminalButton = bottomBar.getComponent(11);
			this.views.openButton = bottomBar.getComponent(12);
			
			this.views.logButton = bottomBar.getComponent(13);
			this.views.modeSelector = bottomBar.getComponent(15);
			this.views.analysisTypeSelector = topBar.getComponent(0);
			
			this.views.showInspectorButton = topBar.getComponent(2);
			this.views.showInspectorButton.setHandler(function(btn,e){
				if (this.views.inspector.isHidden()) {
					this.views.inspector.showBy(btn);
				} else {
					this.views.inspector.hide();				
				}
				
				// var eles=this.Domain.exportToJSON().theElements;
				// var arr=[];
				// for (var i in eles) {
					// if(eles.hasOwnProperty(i) && $D.isDefined(eles[i])) {
						// arr.push(eles[i]);
					// }
				// }
				var arr = sketchit.dataAdapter.getElementList(function(el){
					return el.isSelected;
				});
				sketchit.views.elementView.readDajoata(arr);
				// sketchit.views.elementView.show();
				// sketchit.views.elementList.getStore().loadData(arr);
				// sketchit.views.elementList.show();
// 				
				
				
			},this);
			
			this.views.showAnimatorButton = topBar.getComponent(3);
			this.views.showAnimatorButton.setHandler(function(btn,e){
				if (this.views.animator.isHidden()) {
					this.views.animator.show();
				} else {
					this.views.animator.hide();
				}
			},this);
			
			this.views.showLibraryButton = topBar.getComponent(4);
			this.views.showLibraryButton.setHandler(function(btn,e){
				if (this.views.libraryView.isHidden()) {
					this.views.libraryView.showBy(btn);
				} else {
					this.views.libraryView.hide();
				}
			},this);
			
			// this.views.showAnimatorButton = topBar.getComponent(3);
			//TODO:
			
			// TODO:

			this.views.viewport.on({
				orientationchange : this.onOrientationchange,
				scope : this
			});

			// init canvas handlers
			this.views.canvas.mon(this.views.canvas.el, {
				doubletap : this.onDoubleTap,
				touchmove : this.onTouchMove,
				touchstart : this.onTouchStart,
				touchend : this.onTouchEnd,
				mousewheel : this.onMouseWheel,
				pinchstart : this.onPinchStart,
				pinch : this.onPinch,
				pinchend : this.onPinchEnd,
				scope : this
			});

			// // setting button
			// var showMoreSetting=function(btn){N
// 				
			// };
			this.views.settingButton.setHandler(function(btn,event){
				if (this.views.settingTabs.isHidden()) {
					this.views.settingTabs.showBy(btn);
				} else {
					this.views.settingTabs.hide();
				}
			}, this);
			
			//run button
			// var player = new sketchit.views.AnimateWidget();
			var player = this.views.animator;
			// player.show();
			// player.hide();
			window.player=player;

            this.runAnalysisHandler = function() {
				if (player.isHidden() && this.settings.analysisType === 'Dynamic') {
					player.show();
					var time = "0.020";
					var Dm =this.Domain;
					var tid;
                    // var maxsteps = 1000;
                    // sketchit.Domain.nodeHistoryVolumn = 0;
                    var count = 0;
					var f=function(){
						
						// Dm.loadNodeDispResultAtT(time);
						// time = parseFloat(time) + 0.02;
						// time = time.toFixed(3)+'';
						var dt = 20;
						var buffer_gap = 2;
						var time2 = parseFloat(time) + buffer_gap;
						time2=time2.toFixed(3)+'';
                        // while (!Dm.nodeDispHistory[time] && count < maxsteps) {
                        //     time = time.toFixed(3)+'';
                        //     count ++;
                        // }
                        
						if (Dm.nodeDispHistory[time]) {
							Dm.loadNodeDispResultAtT(time);
							time = parseFloat(time) + dt/1000;
							time = time.toFixed(3)+'';
							sketchit.printMessage("animating...");
							
							
						} else if (parseFloat(time) < 13.98) {
							sketchit.printMessage("buffering...");
						} else {
							sketchit.printMessage("done");
                        }
						
						tid = setTimeout(f,dt);
					};
					var play=function(){
						f();
						player.getComponent(0).setIconClass('ButtonPause');
					};
					var pause=function(){
						clearTimeout(tid);
						player.getComponent(0).setIconClass('ButtonFFW');
					};
					var reset=function(){
						pause();
						time = "0.020";
						Dm.loadNodeDispResultAtT(time);
					};
                    // var 
					
					player.getComponent(0).setHandler(function(){
						if (player.getComponent(0).iconCls==="ButtonFFW") {
							play();
						} else {
							pause();
						}
					},this);
					
					player.getComponent(1).setHandler(function(){
						reset();
					},this);
					
                    // TODO: add a button to clear analysis results
					player.getComponent(2).setHandler(function(){
                        Domain.nodeDispHistory = {};
					},this);
                    
					player.getComponent(3).setHandler(function(){
						player.hide();
					},this);
                    
                    
					// player.getComponent(3).setHandler(function(){
					// 	player.hide();
					// },this);
				}
				this.analyze();
				if (this.settings.analysisType === 'Dynamic') {
				    play();
                } else if (this.settings.analysisType === 'Static'){
                    // this.refresh();
                }
			};
			this.views.runButton.setHandler(this.runAnalysisHandler, this);
			
			// autoScale button					
			this.views.rescaleButton.setHandler(this.autoScale, this);
			
			//mesh button
			this.views.meshButton.setHandler(this.autoMesh, this);
			
			//clear all button
			this.views.clearAllButton.setHandler(this.clearAll, this);
            
            this.cancelSelectButtonHandler = function() {
                this.Domain.mark();
				if(!this.Domain["unselectAll"]()) {
					this.printMessage("do nothing");
					this.Domain.unmark();
				} else {
					this.Domain.group();
				}
            };
            this.views.unselectAllButton.setHandler(this.cancelSelectButtonHandler, this);

            this.deleteButtonHandler = function() {
				this.Domain.mark();
				if(!this.Domain["removeSelectedObjects"]()) {
					this.printMessage("do nothing");
					this.Domain.unmark();
				} else {
					this.Domain.group();
					if(this.settings.autoAnalysis) {
						this.analyze(function() {
						});
					} else {
					}
				}
			};
            this.views.deleteButton.setHandler(this.deleteButtonHandler, this);
			
			//undo button
			this.views.undoButton.setHandler(this.undo, this);
			this.views.undoButton.setDisabled(true);
			
			//redo button
			this.views.redoButton.setHandler(this.redo, this);
			this.views.redoButton.setDisabled(true);
			
			//save button
			this.views.saveButton.setHandler(this.saveScript, this);
			
			//terminal button
			var term = new Ext.Panel({
				floating: true,
				// hideOnMaskTap : false,
				width: 500,
				height: 300,
				style : {
					opacity:0.8
				},
				layout: {
					type : 'vbox',
					align : 'center'
				},
				items: [
				{
		        	xtype : 'panel',
		        	height : 100,
		        	width : 490,
		        	cls : 'card1'
		        },{
		        	xtype : 'textareafield',
		        	width : 490 ,
                    maxRows : 4
		        }]
			});
			term.show();
			
			var commands = "";
			// var maxCmdLen=100;
			// var cmdStore=new Array(maxCmdLen);
			var cmdStore=[];
			var cmdPtr=0;
			var scope = this;
			
			term.getComponent(1).el.dom.onkeypress=function(e){
				// e.preventDefault();
				if (e.keyCode == 13) {
					commands=term.getComponent(1).getValue();
					term.getComponent(0).html=term.getComponent(0).html+"\n"+commands;
					// cmdStore[cmdPtr]=commands;
					cmdStore.push(commands);
					
					// scope.ws.send(commands);
					scope.ws.emit('ops-stdin', commands);
					commands="";
					term.getComponent(1).setValue("");
					cmdPtr=cmdStore.length;
					return false;
					
					
				} else if(e.keyCode == 38) {
					cmdPtr--;
					if (cmdPtr<0) {
						cmpPtr=0;
					}
					commands=cmdStore[cmdPtr];
					term.getComponent(1).setValue(commands);
					return false;

				} else if(e.keyCode == 40) {
					cmdPtr++;
					if (cmdPtr>cmdStore.length){
						cmdPtr=cmdStore.length;
					}
					
					if (cmdPtr<cmdStore.length){
						commands=cmdStore[cmdPtr];
					} else {
						commands="";
					}
					
					term.getComponent(1).setValue(commands);
					return false;
				} else {
                    return false;
                }
			};
			term.hide();
			
			this.views.terminalButton.setHandler(function(btn,e){
				if (!term || term.isHidden()) {
					term.showBy(btn);
				} else {
					term.hide();
				}					
			}, this);
			
			
			//Open button
			// var dm = this.Domain;
		    var importModel = function(){
				// var obj = eval("("+importwindow.getComponent(0).getValue()+")");
                // debugger;
                var str = Ext.getCmp('model_import_field').getValue();
                try {
				    // var obj = eval("("+Ext.getCmp('model_import_field').getValue()+")");
				    var obj = eval("("+ str +")");
                } catch(e) {
                    // debugger;
                    sketchit.ws.emit('ops-stdin',
                                     'cd $root_dir/files/shared/tests;' +
                                     str +
                                     ';json-echo-domain');
                    return;
                }
				sketchit.Domain.buildModelFromJson(obj);
			};

            var loadImg = function() {
                var data = Ext.getCmp('bgimage_src').getValue().split(';');
                var src = data[0];
                if (src !== 'none') {
                    var srcWidth = data[1];
                    var srcHeight = data[2];
                    var x = Ext.getCmp('bgimage_x').getValue();
                    var y = Ext.getCmp('bgimage_y').getValue();
                    var scale = Ext.getCmp('bgimage_scale').getValue();
                    sketchit.loadImageToCanvas(src, x, y, scale * srcWidth, scale * srcHeight);
                } else {
                    sketchit.bgImage = undefined;
                }
            };
            
            var myTabBarTop = {
	            ui: 'light',
	            dock: 'top',
	            baseCls: 'x-tabbar',
	            layout: {
		            pack: 'center',
		            type: 'hbox'
	            }
            };
            
			var importwindow = new Ext.TabPanel({
				floating: true,
				hideOnMaskTap : false,
				hidden:true,
				width: 500,
				height: 400,
				style : {
					opacity:0.8
				}, 
	            tabBar : myTabBarTop,               
                defaults : {
		            xtype : 'panel',
                    layout : {
                        type : 'vbox',
                        align : 'stretch'
                    }
	            },
				// layout: {
					// // type : 'vbox'
				// },
                items : [{
		            title : 'Model Data',
                    items : [{
                        xtype : 'textareafield',
                        id : 'model_import_field',
                        maxRows : 13
                    }, {
                        xtype : 'button',
		        	    ui: 'confirm',
		        	    text : 'import',
		        	    handler : importModel
                    }]
	            },{
		            title : 'Background Image',
                    items : [{
                        id : 'bgimage_src',
                        xtype : 'selectfield',
                        options : [
                            {text : 'Eiffel Tower', value : 'bgimages/eiffel_tower.jpg;220;407'},
                            {text : 'Piping System', value : 'bgimages/piping.png;881;437'},
                            {text : 'BNSC Test Building', value : 'bgimages/bnsc_struct_model.jpg;1059;1766'},
                            {text : 'Rainbow Bridge', value : 'bgimages/rainbow_bridge.jpg;640;480'},
                            {text : 'Roof truss', value : 'bgimages/roof_truss.jpg;640;480'},
                            {text : 'Metal Building System', value : 'bgimages/metal_building_systems.jpg;2368;1564'},
                            {text : 'None', value : 'none;0;0'}
                        ]
                    },{
                        id : 'bgimage_x',
                        xtype : 'textfield',
                        label : 'x',
                        value : 100
                    },{
                        id : 'bgimage_y',
                        xtype : 'textfield',
                        label : 'y',
                        value : 100
                    },{
                        id : 'bgimage_scale',
                        xtype : 'textfield',
                        label : 'scale',
                        value : 1.0
                    },{
                        xtype : 'button',
		        	    ui: 'confirm',
		        	    text : 'load',
		        	    handler : loadImg
                    }]
	            }]
				// items: [{
		        // 	xtype : 'textareafield',
                //     maxRows : 15
		        // },{
		        // 	xtype : 'button',
		        // 	ui: 'confirm',
		        // 	text : 'import',
		        // 	handler : importModel
		        // }]
			});
			
			this.views.openButton.setHandler(function(btn,e){
				if (importwindow.isHidden()) {
					importwindow.showBy(btn);
				} else {
					importwindow.hide();
				}					
			}, this);

			//log button
			this.views.logButton.setHandler(this.showLog, this);
			
			//mode toggle button
			this.views.modeSelector.on({
				toggle : function() {
					this.settings.mode = this.views.modeSelector.getPressed().text;
				},
				scope : this
			});
			
			//analysisType toggle button
			this.views.analysisTypeSelector.on({
				toggle : function() {
					this.settings.analysisType = this.views.analysisTypeSelector.getPressed().text;
					if (this.settings.analysisType == 'Static') {
						this.settings.autoAnalysis = true;
                        this.settings.deformationScale = 300;
					} else {
						this.settings.autoAnalysis = false;
                        this.settings.deformationScale = 450;
                        					}
				},
				scope : this
			});

			//init model Domain
			this.Domain = new $D.Domain();
			window.Domain = this.Domain;

			//init Renderer
			this.Renderer = new $D.Renderer(document.getElementById('workspace'));
			window.Renderer = this.Renderer;
            // debugger;

            
            this.loadImageToCanvas = function(src, x, y, w, h) {
                var img = new Image();
                img.src = src;
                // debugger;
                this.bgImage = {
                    image : img,
                    x : x || 100,
                    y : y || 100,
                    w : w || 400,
                    h : h || 400
                };
            };
            
            // this.loadImageToCanvas('bgimages/bnsc_struct_model.jpg', 100, 100, 0.5);
            
			var ws;
			function get_appropriate_ws_url() {
				var pcol;
				var u = document.URL;
				if(u.substring(0, 5) == "https") {
					pcol = "wss://";
					u = u.substr(8);
				} else {
					pcol = "ws://";
					if(u.substring(0, 4) == "http") {
						u = u.substr(7);
					}
				}
				u = u.split('/');
			
				// return pcol + u[0] + ":8080/OpenSees";
				return pcol + u[0] + ":8080";
				// return pcol + u[0];
			}
			
			// if(BrowserDetect.browser == "Firefox") {
				// ws = new MozWebSocket(get_appropriate_ws_url(), "ops-protocol");
			// } else {
				// ws = new WebSocket(get_appropriate_ws_url(), "ops-protocol");
			// }
			// ws = new WebSocket(get_appropriate_ws_url(), "ops-protocol");
			// ws = new WebSocket(get_appropriate_ws_url());
			ws = io.connect(get_appropriate_ws_url());
            window.ws = ws;
            ws.on('connect', function() {
                sketchit.printMessage('connect success!!');
                ws.emit('ops-create-interp', 'xxx');
                ws.emit('ops-stdin',
                        'set root_dir [pwd];' +
                        'cd $root_dir/files/shared/tests;' +
                        'puts [pwd];' +
                        'model basic -ndm 2 -ndf 3;' + 
                        'geomTransf Linear 1;' + 
                        'geomTransf PDelta 2;' + 
                        'geomTransf Corotational 3;' + 
                        '');
            });

            window.ops = function(str) {
                ws.emit('ops-stdin', str);
            };

            ws.on('ops-stdout', function(m) {
                console.log('ops-stdout:\n', m);
            });

            ws.on('ops-stderr', function(data) {
                console.log('ops-stderr:\n', data);
            });
            
            ws.on('ops-server-closed', function(data) {
                console.log('OpenSees server closed. Exit code: ' + data.code
                            + ', signal: ' + data.signal);
                ws.emit('ops-create-interp', 'xxx');
                
            });  

            var buffer = '';
            var loadJSON = function(json) {
                if (json["time"] && json["disp"]) {
                    if (Ext.isArray(json)) {
                        for (var i = -1, len = json.length; ++i < len;) {
                            var time = parseFloat(json[i]["time"]).toFixed(3);
                            sketchit.Domain.nodeDispHistory[time] = json[i]["disp"]; 
                        }
                        sketchit.printMessage("load displacement");
                    } else {
                        var time = parseFloat(json["time"]).toFixed(3);
                        sketchit.Domain.nodeDispHistory[time] = json["disp"];
                        sketchit.Domain.nodeDispHistoryVolumn++;
                    }
                    $D.apply(sketchit.Domain.nodeDispHistory, buffer);
                    sketchit.Domain.deformationAvailable = true;
                } else if (json["theNodes"] && json["theElements"]) {
                    sketchit.printMessage("load model");
                    // sketchit.Domain.buildModelFromJson(json);
                    sketchit.Domain.importModelFromServer(json);
                }
            };
            // var maxNumOfChunks = 100;
            // var count = 0;
            ws.on('ops-json-domain', function(data) {
                var obj;
                var temp = data.split(/\|+/);
                if(temp[0] === '') {
                    temp.shift();
                }
                for (var i = 0, len = temp.length; i < len; i++) {
                    if (i === 0){
                        obj = eval(buffer.concat(temp[i]));
                        loadJSON(obj);
                    } else if (i === len -1){
                        buffer = temp[i];
                    } else {
                        obj = eval(temp[i]);
                        loadJSON(obj);
                    }
                }
                
            });

			// ws = new WebSocket(get_appropriate_ws_url());
		
			this.ws=ws;
			this.Domain.setConnection(ws);

			this.settings = {};
			$D.apply(this.settings, defaultSettings);

            
            var toggle_button_id_setting_field_map = {
                'toggle_structure' : 'showStructure',
                'toggle_deformation' : 'showDeformation',
                'toggle_moment' : 'showMoment',
                'toggle_node' : 'showNodes',
                'toggle_node_tag' : 'showNodeId',
                'toggle_element_tag' : 'showElementId',
                'toggle_element_direction' : 'showLineElementDirection',
                'toggle_loads' : 'showLoads',
                'toggle_constraints' : 'showConstraints',
                'toggle_bgImage' : 'showBgImage'
            };

            this.views.settingTabs.on('show', function() {
                for (var i in toggle_button_id_setting_field_map) {
                    var val;
                    if(toggle_button_id_setting_field_map.hasOwnProperty(i)) {
                        val = sketchit.settings[toggle_button_id_setting_field_map[i]];
                        Ext.getCmp(i).setValue(val ? 1 : 0);
                        if (Ext.getCmp(i).__initialized !== true) {
                            Ext.getCmp(i).on('change', (function() {
                                var field = toggle_button_id_setting_field_map[i];
                                var self = Ext.getCmp(i);
                                return function() {
                                    sketchit.settings[field] = !!self.getValue();
                                };
                            })(i));
                            Ext.getCmp(i).__initialized = true;                        
                        }
                    }
                }                
            });

			this.inputStrokes = [];
			this.logs = [];
			this.shapeRecognizer = new DollarRecognizer();
			this.featureExtractor = new featureExtractor();
			// this.initHandlers();
			this.setCanvasPosition(0, this.views.topBar.getHeight(), this.views.viewport.getWidth(), this.views.viewport.getHeight() - this.views.topBar.getHeight() - this.views.bottomBar.getHeight());
			this.resetViewPort();
			this.refresh();
			
	    },
	    
	    enterInteractiveMode : function() {
	    	this.settings.autoAnalysis = true;
	    	this.settings.autoDeformationScale = false;
	    },
	    
	    exitInteractiveMode : function() {
	    	this.settings.autoAnalysis = false;
	    },
	    
		loadSettingsToSettingTabs : function() {
			var str = "";
			var S = this.settings;
			var value;
			for (var key in S) {
				value = S[key];
				if ($D.isString(value)) {
					value = "\""+value+"\"";
				}
				str += key+" : "+ value+ ",\n";
			}
			str = str.slice(0,-2);				
			this.views.settingTabs.getComponent(4).getComponent(0).setValue(str);
		},
		applySettingsFromSettingTabs : function() {
			var temp = eval("({"+this.views.settingTabs.getComponent(4).getComponent(0).getValue()+"})");
			$D.apply(this.settings,temp);
			console.log("settings",temp);
		},
		getCanvasCoordFromViewPort : function(p) {
			return {
				X : (p.X - this.settings.viewPortShiftX) / this.settings.viewPortScale,
				Y : (p.Y - this.settings.viewPortShiftY) / this.settings.viewPortScale
			};
		},
		getViewPortCoordFromCanvas : function(p) {
			return {
				X : p.X * this.settings.viewPortScale + this.settings.viewPortShiftX,
				Y : p.Y * this.settings.viewPortScale + this.settings.viewPortShiftY
			};
		},
		getViewPortCoordFromPage : function(p) {
			return {
				X : p.X - this.canvasUpLeftX,
				Y : this.canvasHeight - p.Y + this.canvasUpleftY
			};
		},
		getCanvasCoordFromPage : function(p) {
			return {
				X : (p.X - this.canvasUpLeftX - this.settings.viewPortShiftX) / this.settings.viewPortScale,
				Y : (this.canvasHeight - p.Y + this.canvasUpleftY - this.settings.viewPortShiftY) / this.settings.viewPortScale
			};
		},
		setCanvasPosition : function(upleftX, upleftY, width, height) {
			this.canvasUpLeftX = upleftX;
			this.canvasUpleftY = upleftY;
			this.canvasWidth = width;
			this.canvasHeight = height;
		},
		initCT : function() {
			this.Renderer.setTransform(1.0, 0, 0, -1.0, 0, this.canvasHeight);
		},
		applyViewPortTransform : function() {
			this.Renderer.transform(this.settings.viewPortScale, 0, 0, this.settings.viewPortScale, this.settings.viewPortShiftX, this.settings.viewPortShiftY);
		},
		resetViewPort : function() {
			this.settings.viewPortScale = 1.0;
			this.settings.viewPortShiftX = 0.0;
			this.settings.viewPortShiftY = 0.0;
		},
		deltaTransform : function(dScale, dShiftX, dShiftY) {
			var R = this.Renderer;
            var S = this.settings;
			R.save();
			R.transform(dScale, 0, 0, dScale, dShiftX, dShiftY);
			this.deltaScale = dScale;
			this.deltaShiftX = dShiftX;
			this.deltaShiftY = dShiftY;
			this.clearScreen();
			R.save();
			this.initCT();
			this.drawMessage();
			R.restore();
			this.drawDomain();
			R.restore();
		},
		recordDeltaTransform : function() {
			var S = this.settings;
			S.viewPortShiftX = S.viewPortScale * this.deltaShiftX + S.viewPortShiftX;
			S.viewPortShiftY = S.viewPortScale * this.deltaShiftY + S.viewPortShiftY;
			S.viewPortScale = S.viewPortScale * this.deltaScale;
		},
		applyInputStrokeStyle : function(scale) {
			var R = this.Renderer, S = this.settings;
			R.strokeStyle = S.inputStrokeStyle;
			R.lineWidth = S.inputStrokeWidth / scale;
		},
		applyGridStyle : function(scale) {
			var R = this.Renderer, S = this.settings;
			R.strokeStyle = S.gridLineStyle;
			R.lineWidth = S.gridLineWidth / scale;
		},
		getAutoDeformationScale : function(maxDispOnScreen) {
			var a = $D.getAbsMax(this.Domain.theNodes, "dispX"), b = $D.getAbsMax(this.Domain.theNodes, "dispY"), m = a > b ? a : b;
			return maxDispOnScreen / m;
		},
		getAutoMomentScale : function(maxDispOnScreen) {
			var a = $D.getAbsMax(this.Domain.theElements, "maxMoment");
			console.log("maxM", a);
			return maxDispOnScreen / a;
		},
		clearScreen : function() {
			var R = this.Renderer;
			R.save();
			this.initCT();
			R.fillStyle = this.settings.canvasBgColor;
			R.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
			R.restore();
		},
		drawObjectStore : function(store, fn) {
			var iter = $D.iterate;
			var args = Array.prototype.slice.call(arguments, 2);
			iter(store, function(e) {
				e[fn].apply(e, args);
			});
		},
		drawMessage : function() {
			var R = this.Renderer, //
			S = this.settings, //
			msg = this.screenMessage;
			if($D.isDefined(msg)) {
				R.save();
				R.font = S.messageTextFont;
				R.fillStyle = S.messageTextFillStye;
				R.transform(1, 0, 0, -1, 0, 0);
				R.fillText(msg, S.messageBoxPositionX, -S.messageBoxPositionY);
				R.restore();
			}

            R.save();
			R.font = S.messageTextFont;
            var connected = ws.socket.connected;
			if(connected === true) {
                R.fillStyle = "green";
                msg = "server connected";
			} else {
                R.fillStyle = "red";
                msg = "server disconnected";
			}
			R.transform(1, 0, 0, -1, 0, 0);
			R.fillText(msg, 20, -(this.canvasHeight-10));
			R.restore();

            R.save();
            var scale = S.deformationScale;
            R.font = S.messageTextFont;
            R.fillStyle = 'black';
			R.transform(1, 0, 0, -1, 0, 0);
            
            msg = "grid : " + S.grid + " in; deformation scale : " + scale.toFixed(2);
			R.fillText(msg, 20, -(this.canvasHeight-25));
            
			R.restore();

            if (S.defaultLineELementType === 'ElasticBeamColumn') {
                R.save();
                R.font = S.messageTextFont;
                R.fillStyle = 'rgba(0, 0, 0, 0.4)';
			    R.transform(1, 0, 0, -1, 0, 0);

                msg = "element type : " + S.defaultLineELementType;
			    R.fillText(msg, 20, -(this.canvasHeight-40));
                
                msg = "A : " + S.defaultElasticBeamA + ' in^2';
			    R.fillText(msg, 20, -(this.canvasHeight-55));

                msg = "I : " + S.defaultElasticBeamI + ' in^4';
			    R.fillText(msg, 20, -(this.canvasHeight-70));

                msg = "E : " + S.defaultElasticBeamE + ' klbf/in^2';
			    R.fillText(msg, 20, -(this.canvasHeight-85));

                msg = "Density : " + S.defaultElasticBeamDensity.toExponential(5) + ' kips/in^3';
			    R.fillText(msg, 20, -(this.canvasHeight-100));
                
			    R.restore();
            }


		},

        // drawModelSettings :
		drawFrameRate : function() {
			var R = this.Renderer, //
			S = this.settings, //
			msg = "fps:"+fps.toFixed(2);
			if($D.isDefined(msg)) {
				R.save();
				R.font = S.messageTextFont;
				R.fillStyle = S.messageTextFillStye;
				R.transform(1, 0, 0, -1, 0, 0);
				// R.fillText(msg, S.frameRatePositionX, -S.frameRatePositionY);
				R.fillText(msg, this.canvasWidth-80, -(this.canvasHeight-10));
				R.restore();
			}
		},
		drawDomain : function() {
			var R = this.Renderer, //
			S = this.settings, //
			vps = S.viewPortScale, //
			dfs = S.deformationScale, //
			draw = this.drawObjectStore, //
			domain = this.Domain, //
			nodes = domain.theNodes, //
			eles = domain.theElements;
            var bgImage = this.bgImage;

            if (this.bgImage && S.showBgImage) {
                R.save();
                R.transform(1.0, 0.0, 0.0, -1.0, 0.0, this.canvasHeight);
                R.drawImage(bgImage.image, bgImage.x, bgImage.y, bgImage.w, bgImage.h);
                R.restore();
            }
            if(S.showGrid) {
				var c1 = this.getCanvasCoordFromViewPort({
					X : 0,
					Y : 0
				}), c2 = this.getCanvasCoordFromViewPort({
					X : this.canvasWidth,
					Y : this.canvasHeight
				});
				this.applyGridStyle(vps);
				R.drawGrid(S.grid, S.grid, c1.X, c2.X, c1.Y, c2.Y);
			}
			
			if (S.showStructure) {
				draw(eles, "display", R, vps);
			}
			if (S.showConstraints) {
				draw(domain.theSPCs, "display", R, vps);
			}
			if (S.showLoads) {
				draw(domain.thePatterns[1].Loads, "display", R, vps);
				draw(domain.thePatterns[2].Loads, "display", R, vps);
			}
			if (S.showNodes) {
				draw(nodes, "display", R, vps);
			}

			// if(S.showDeformation) {
			if(this.Domain.isDeformationAvailable() && S.showDeformation) {
				if (S.autoAnalysis) {
					domain.loadNodeDispResultAtT("1.000");
				}
				
				if(sketchit.settings.autoDeformationScale) {
				sketchit.settings.deformationScale = sketchit.getAutoDeformationScale(sketchit.settings.maxDeformationOnScreen);
					sketchit.settings.autoDeformationScale = false;
				} else {
					// sketchit.settings.deformationScale = defaultSettings.deformationScale;
					// sketchit.settings.autoDeformationScale = false;
				}
				dfs = S.deformationScale;
				
				draw(eles, "displayDeformation", R, vps, dfs, S.deformationResolution);
				draw(nodes, "displayDeformation", R, vps, dfs);
			}
			//}
			if(S.showMoment) {
			// if(this.Domain.deformationAvailable && S.showMoment) {
				draw(eles, "displayMoment", R, vps, S.momentScale, S.momentResolution);
			}
			if(S.showNodeId) {
				draw(nodes, "displayTag", R, vps);
			}
			if(S.showElementId) {
				draw(eles, "displayTag", R, vps);
			}
			if(S.showLineElementDirection) {
				draw(eles, "displayDirection", R, vps);
			}

            
		},
		
		drawInputStrokes : function() {
			for (var i=1; i < this.inputStrokes.length; i++) {
				this.applyInputStrokeStyle(this.settings.viewPortScale);
				this.Renderer.drawLine(this.inputStrokes[i - 1], this.inputStrokes[i]);
			};					
		},
		
		refresh : function() {
			// console.log('refresh')
			this.clearScreen();
			this.initCT();

            
            
            
			if (this.Domain.readyToRender){
				this.applyViewPortTransform();
				this.drawDomain();
			}
			this.drawInputStrokes();

            this.Renderer.save();
            this.initCT();
            this.drawMessage();
			this.drawFrameRate();
            this.Renderer.restore();

            
            
			var thisFrameFPS = 1000 / ((now=new Date) - lastUpdate);
		  	fps += (thisFrameFPS - fps) / fpsFilter;
		  	lastUpdate = now;
		  	var temp = this;
		
		  	setTimeout( function(){
                if (!temp.pause) {
		  		    temp.refresh();
                }
		  	}, 1000/temp.settings.fps);
		},
		onOrientationchange : function() {
			this.setCanvasPosition(0, this.views.topBar.getHeight(), this.viewport.getWidth(), this.viewport.getHeight() - this.views.topBar.getHeight() - this.views.bottomBar.getHeight());
			this.resetViewPort();
			// this.refresh();
		},
		onDoubleTap : function(e, el, obj) {
			this.resetViewPort();
			// this.refresh();
		},
		animate : function(condition, fn, dt) {
			setTimeout(function(scope) {
				fn.call(scope);
				if(condition.call(scope)) {
					scope.animate(condition, fn, dt);
				}
			}, dt, this);
		},
		animate : function(condition, fn, dt) {
			var id = setInterval(function(scope) {
				fn.call(scope);
				if(!condition.call(scope)) {
					clearInterval(id);
				}
			}, dt, this);
		},
		onTouchStart : function(e, el, obj) {
			// console.log("touch start!",e)
			var P = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			});
			this.touchCurrentX = P.X;
			this.touchCurrentY = P.Y;
			this.touchStartX = P.X;
			this.touchStartY = P.Y;
			this.shiftKey = e.event.shiftKey;
			if(this.settings.mode === "move") {
				if((e.event.type === "mousedown" && e.event.button == 1) || e.event.shiftKey) {

				} else {
					this.settings.touchMoveAnimation = true;
					this.analyze();
					// this.animate(function() {
						// return this.settings.touchMoveAnimation;
					// }, function() {
						// if(this.settings.autoAnalysis) {
							// this.analyze(function() {
								// // this.refresh();
							// });
						// } else {
							// // this.refresh();
						// }
					// }, 1000 / this.settings.touchMoveFps);

				}
			} else {
				this.inputStrokes = [];
				this.inputStrokes.push(P);
			}
		},
		onTouchMove : function(e, el, obj) {
			var P = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			});
			var S = this.settings;
			if(e.event.type === "mousemove" && (e.event.button == 1 || e.event.shiftKey)) {
				this.deltaTransform(1, P.X - this.touchStartX, P.Y - this.touchStartY);
				this.recordDeltaTransform();
			} else {
				if(this.settings.mode === "move" && this.settings.touchMoveAnimation === true) {
					duplicateNodes = false;
					for(i in this.Domain.selectedNodes) {
						var np1 = this.Domain.snapToNode(this.Domain.selectedNodes[1], S.moveSnapToNodeThreshold);
						if(np1.capture) {
							duplicateNodes = true;
						}
					}
					
					
					// if($D.isDefined(this.Domain.selectedNodes[1])) {
						// var np1 = this.Domain.snapToNode(this.Domain.selectedNodes[1], S.moveSnapToNodeThreshold);
						// if(np1.capture) {
							// isAnalysisReadyToRun = false;
						// } else {
							// isAnalysisReadyToRun = true;
						// }
					// }
					this.Domain["moveSelectedObjects"](P.X - this.touchCurrentX, P.Y - this.touchCurrentY);
					if (!duplicateNodes && isAnalysisReadyToRun) {
						this.analyze();
					}
				
				
				
				} else {
					this.inputStrokes.push(P);
					// var l = this.inputStrokes.length;
					// 							this.applyInputStrokeStyle(this.settings.viewPortScale);
					// 							this.Renderer.drawLine(this.inputStrokes[l - 2], this.inputStrokes[l - 1]);
				}
			}
			this.touchCurrentX = P.X;
			this.touchCurrentY = P.Y;
		},
		onTouchEnd : function(e, el, obj) {
			// this.inputStrokes = [];
			if(this.settings.mode === "move") {
				this.settings.touchMoveAnimation = false;
			}
			if((e.event.type === "mouseup" && e.event.button == 1) || (e.event.type === "mouseup" && this.shiftKey)) {
				this.recordDeltaTransform();
				// this.refresh();
			} else if(e.touches.length === 0 || e.touches.length === 1) {
				switch (this.settings.mode) {
				  case "move":
					this.afterMovingObjects();
					break;
				  case "select":
					this.sketchSelect();
					break;
				  case "draw":
				  case "load":
					var gesture;
					var command;
					// gesture = this.shapeRecognizer.Recognize(this.inputStrokes, false);
					gesture = this.shapeRecognizer.Recognize(this.inputStrokes, true);
					console.log("gesture:" + gesture.name, gesture);
                    this.printMessage('gesture: ' + gesture.name);
					if(gesture) {
						gesture = this.featureExtractor.extractFeature(gesture);
						// console.log("gesture:",gesture.name,gesture);
						// command = this.gestureToCommandHandler(gesture);
						// console.log("command:",gesture.name,gesture);
						// gesture=this.featureExtractor.extract(gesture);
						// console.log("shape:",shape);
						command = this.sketchVocabulary[this.settings.mode][gesture.name];
                        var data = Ext.apply(gesture.data, gesture.features);
						if(command && this.sketchHandlers[command]) {
							// this.sketchHandlers[command].call(this, gesture.data);
							this.sketchHandlers[command].call(this, data);
						} else {
							this.sketchHandlers["noSuchGesture"].call(this);
						}
						
					} else {
						this.sketchHandlers["noSuchGesture"].call(this);
					}
					
					

					break;
				default:
					break;

				}
				this.Domain.group();
				this.Domain.unmark();

			}
			this.inputStrokes = [];
			this.shiftKey = e.event.shiftKey;
		},
		afterMovingObjects : function() {
			this.beforeUndoableCommand();
			var count;
			var dx = this.touchCurrentX - this.touchStartX;
			var dy = this.touchCurrentY - this.touchStartY;
			var S = this.settings;
			count = this.Domain["transitSelectedObjects"](dx, dy);
			var changed = count > 0 ? true : false;
			// test merge
			if($D.isDefined(this.Domain.selectedNodes[1])) {
				var np1 = this.Domain.snapToNode(this.Domain.selectedNodes[1], S.moveSnapToNodeThreshold);
				if(np1.capture) {
					this.Domain.mergeNodes(this.Domain.selectedNodes[1], np1.node);
				}
			}
			if(count) {
				var msg = "move " + count + " objects, dx = " + dx + " dy = " + dy;
			}
			this.afterUndoableCommand(changed, changed, msg);
			// this.refresh();
		},
		sketchSelect : function() {
			this.beforeUndoableCommand();
			var data = this.shapeRecognizer.Recognize(this.inputStrokes, false).data;
			var d = $D.distance(data.from, data.to);
			var l = data.PathLength;
			var tags;
			if(d / l > this.settings.circleSelectThreshold) {
				tags = this.Domain["intersectSelect"]({
					"curve" : data.ResamplePoints
				});
			} else {
				if(l >= this.settings.clickSelectThreshold) {
					tags = this.Domain["circleSelect"]({
						"poly" : data.ResamplePoints
					});
				}
			}
			if(tags) {
				var selectCount = this.Domain.count(tags.select);
				var unSelectCount = this.Domain.count(tags.unselect);
				var msg = "";
				if(selectCount > 0) {
					msg += "select " + selectCount + " objects; ";
				}
				if(unSelectCount > 0) {
					msg += "unselect " + unSelectCount + " objects; ";
				}
			}

            this.printMessage(msg);
			this.afterUndoableCommand(tags, msg);
			// this.refresh();
		},
		beforeUndoableCommand : function() {
			this.Domain.mark();
		},
		afterUndoableCommand : function(domainTouched, domainChanged, isUndoable) {
			var touched = $D.isDefined(domainTouched)? domainTouched:true;
			var changed = $D.isDefined(domainChanged)? domainChanged:true;
			var undoable = $D.isDefined(isUndoable)? isUndoable:true;
			this.Domain.group();
			if(touched) {
				if(changed) {
					if(!undoable) {
						this.Domain._timeline.pop();
						this.Domain._head--;
					} else {
						this.views.undoButton.setDisabled(false);
						this.views.redoButton.setDisabled(true);	
					}
				} else {
					this.Domain.undo();
					this.Domain._timeline.pop();
				}
			}
		},
		afterReanalyzeRequiredCommand: function() {
			var S = this.settings;
			if (S.autoAnalysis) {
				if(this.Domain.isReadyToRun()) {
					this.analyze(function(){
						// this.refresh();
					});
				}
			} else {
				// this.refresh();
			}
		},
		printMessage : function(msg) {
			this.screenMessage = msg;
		},
		logMessage: function(msgobj) {
			this.logs.push(msgobj);
		},
		meshLine : function(l,n) {
			this.beforeUndoableCommand();
			var touched = true;
			var changed = true;
			var dm = this.Domain;
			var arr = [];
			if ($D.isArray(n)) {
				arr = n;
			} else {
				for (var i = 1; i < n; i++) {
					arr.push(i/n);
				}
			}
			dm.splitLineElement (l, arr, true);
			var msg;
			if(changed) {
				msg = "mesh line "+ l.id + " into " + n + " segments";
				this.logMessage(msg);
			} else {
				msg = "do nothing";
			}
			this.afterUndoableCommand(touched,changed,true);
			this.printMessage(msg);
			this.afterReanalyzeRequiredCommand();
		},
		
		autoMesh: function(){
			this.beforeUndoableCommand();
			var touched = true;
			var changed = true;
			var dm = this.Domain;
			var sz = this.settings.autoMeshSize;
			var ratio = [];
			var n;
			var l;
			var ls = [];
			
			for (var i in dm.theElements) {
				if(dm.theElements.hasOwnProperty(i)) {
					ls.push(dm.theElements[i]);
				}
			};
			
			for (i=0; i < ls.length; i++) {
				l = ls[i];
				n = Math.round(l.getLength()/sz);
				ratio = [];
				for (var j=0; j < n-1; j++) {
					ratio.push((j+1)/n);
				};
				dm.splitLineElement (l, ratio, true);
			};

			var msg;
			if(changed) {
				msg = "auto mesh with size = "+ sz;
				this.logMessage(msg);
			} else {
				msg = "do nothing";
			}
			this.afterUndoableCommand(touched,changed,true);
			this.printMessage(msg);
			this.afterReanalyzeRequiredCommand();
			
		},
		
		sketchVocabulary : {
			"draw" : {
				"line" : "drawLine",
				"triangle" : "drawTriangle",
				"circle" : "drawCircle",
                "arrow" : "loadLine",
                "pigtail_1" : "undo",
                "pigtail_2" : "redo",
                "letter_m" : "meshSelectedBlocks",
                "squareBracket" : "loadSquareBracket",
                "check" : "runAnalysis",
                "delete" : "deleteSelectedObjects",
                "cancel" : "cancelSelection"
			}
            // ,
			// "load" : {
			// 	"line" : "loadLine",
			// 	"squareBracket" : "loadSquareBracket",
			// 	"arrow" : "loadLine"
			// }
		},
		sketchHandlers : {

			"noSuchGesture" : function() {
				var msg = "no such gesture :-(";
				// this.logMessage(msg);
				this.printMessage(msg);
				// this.refresh();
			},
            "undo" : function() {
                this.undo();
                this.printMessage('undo');
            },
            
            "redo" : function() {
                this.redo();
                this.printMessage('redo');                
            },

            "runAnalysis" : function() {
                this.runAnalysisHandler(); 
                this.printMessage('run analysis');               
            },

            "meshSelectedBlocks" : function() {
                this.autoMesh();
                this.printMessage('auto mesh');                
            },

            "deleteSelectedObjects" : function() {
                this.deleteButtonHandler();
                this.printMessage('delete selected objects');                
            },

            "cancelSelection" : function() {
                this.cancelSelectButtonHandler();
                this.printMessage('cancel selection');                
            },

			"drawLine" : function(data) {
				this.beforeUndoableCommand();
				var dm = this.Domain;
				var S = this.settings;
				var fx = data.from.X;
				var fy = data.from.Y;
				var tx = data.to.X;
				var ty = data.to.Y;
				var n1 = dm.createNode(fx, fy);
				var n2 = dm.createNode(tx, ty);
				var changed = false;
				var touched = true;
				var nt = S.snapToNodeThreshold / S.viewPortScale;
				var lt = S.snapToLineThreshold / S.viewPortScale;
				var at = S.autoMergeNodeOnLineThreshold / S.viewPortScale;
				if(S.snapToNode) {
					var np1 = dm.snapToNode(n1, nt);
					var np2 = dm.snapToNode(n2, nt);
					// var np1 = dm.snapToNode(n1, S.snapToNodeThreshold);
					// var np2 = dm.snapToNode(n2, S.snapToNodeThreshold);
					if(np1.capture && np2.capture) {
						if(np1.nodeId == n2.id || np2.nodeId == n1.id || np1.nodeId == np2.nodeId) {
							// dm.removeLineElement(e);
							// dm.removeNode(n1);
							// dm.removeNode(n2);
							changed = false;
						} else {
							n1 = dm.mergeNodes(n1, np1.node);
							n2 = dm.mergeNodes(n2, np2.node);
							// touched = true;
							changed = true;
						}
					} else if(np1.capture && !np2.capture) {
						n1 = dm.mergeNodes(n1, np1.node);
						// touched = true;
						changed = true;
					} else if(!np1.capture && np2.capture) {
						n2 = dm.mergeNodes(n2, np2.node);
						// touched = true;
						changed = true;
					} else {
						// touched = true;
						changed = true;
					}
				};
				
				if (!S.snapToNode || (!(np1.capture && np2.capture) && S.snapToLine)) {
					var nl1 = dm.snapToLine(n1, lt);
					var nl2 = dm.snapToLine(n2, lt);
					// var nl1 = dm.snapToLine(n1, S.snapToLineThreshold);
					// var nl2 = dm.snapToLine(n2, S.snapToLineThreshold);
					if ((nl1.capture && nl2.capture) && (nl1.lineId == nl2.lineId)) {
						changed = false;
					} else {
						if (nl1.capture && Math.abs(nl1.ratio -1.0) > 0.01 && Math.abs(nl1.ratio) > 0.01){
							var nonnl1 = dm.splitLineElement(nl1.line,nl1.ratio);
							n1 = dm.mergeNodes(n1,nonnl1);
						}; 
						if (nl2.capture && Math.abs(nl2.ratio -1.0) > 0.01 && Math.abs(nl2.ratio) > 0.01) {
							var nonnl2 = dm.splitLineElement(nl2.line,nl2.ratio);
							n2 = dm.mergeNodes(n2,nonnl2);
						};
					};
				};
				
				if (S.snapToGrid) {
					var grid = S.grid;
					dm.transitNode(["theNodes",n1.id], Math.round(n1.X / grid) * grid - n1.X, Math.round(n1.Y / grid) * grid - n1.Y); 
					dm.transitNode(["theNodes",n2.id], Math.round(n2.X / grid) * grid - n2.X, Math.round(n2.Y / grid) * grid - n2.Y); 
					$D.iterate(dm.theNodes,function(n){
						if (n.id != n1.id && $D.distance(n,n1) < 0.01) {
							n1 = dm.mergeNodes(n1, n);
						};
						if (n.id != n2.id && $D.distance(n,n2) < 0.01) {
							n2 = dm.mergeNodes(n2, n);
						};
						
					});
					// dm.transitNode(n2.id, n2.X - Math.round(n2.X / grid) * grid, n2.Y - Math.round(n2.Y / grid) * grid); 
				};
                
				if (n1.id != n2.id) {
                    // debugger;
                    var eleProp = {};
                    if (S.defaultLineELementType === 'ElasticBeamColumn') {
                        eleProp.geomTransf = dm.theGeomTransfs[S.defaultGeomTransfId];
                        eleProp.A = S.defaultElasticBeamA;
                        eleProp.I = S.defaultElasticBeamI;
                        eleProp.E = S.defaultElasticBeamE;
                        eleProp.density = S.defaultElasticBeamDensity;
                    }
					// var e = dm.createLineElement(S.defaultLineELementType, n1, n2, {
					// 	geomTransf : dm.theGeomTransfs[S.defaultGeomTransfId]
					// });
                    
					var e = dm.createLineElement(S.defaultLineELementType, n1, n2, eleProp);

				} else {
					changed = false;
				}
				
				if (changed) {
					var ratios = [];
					var ns = [];
					$D.iterate(dm.theNodes,function(n){
						if (n.id != e.getFrom().id && n.id != e.getEnd().id) {
							var nl = dm.snapToLine(n, at);
							if (nl.capture && nl.lineId == e.id) {
							// if (nl.capture) {
								ratios.push(nl.ratio);
								ns.push(n);
							};
							
						}
					});
					if (ns.length > 0) {
						var narr = dm.splitLineElement(e, ratios);
						if (!$D.isArray(narr)) {
							narr = [narr];
						};
						for (var i=0; i < ns.length; i++) {
							dm.mergeNodes(ns[i],narr[i]);
							if (ns[i].SPC) {
								dm.set(["theNodes", narr[i].id, "SPC"], ns[i].SPC);
								dm.set(["theSPCs", ns[i].SPC.id, "node"], narr[i]);
							}
						};
					};
					
				};
				
				var msg;
				if(changed) {
					msg = "add a " + e.className;
					this.logMessage(msg);
				} else {
					msg = "nodes are merged, abort";
				}
				this.afterUndoableCommand(touched,changed,true);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			"drawTriangle" : function(data) {
				this.beforeUndoableCommand();
				var dm = this.Domain;
				var S = this.settings;
				var n = dm.createNode(data.from.X, data.from.Y);
				var changed;
				var np;
				var nt = S.SPCSnapToNodeThreshold/S.viewPortScale;
				var lt = S.SPCSnapToLineThreshold/S.viewPortScale;
				
				var temp = dm.snapTo4Direction(data.IndicativeAngle, S.SPCSnapToDirectionThreshold);
				// if(temp.capture) {
					// angle = temp.angle;
					// direction = temp.direction;
				// }
				// temp = {
					// node : n,
					// angle : angle,
					// show : show
				// };
				// if($D.isDefined(direction)) {
					// temp.direction = directiraon;
				// }
				// spc = new SPC(temp);
				// this.addComponent({
					// storeSelector : "theSPCs",
					// item : spc
				// });
				// //this.run("addAComponent", spc, this.theSPCs);
				// this.set("theNodes." + n.id + ".SPC", spc);
				
				
				var spc = dm.createSPC(n,{
					direction: temp.dir,
					angle : temp.angle
				});
				if(S.SPCSnapToNode) {
					// np = dm.snapToNode(n, S.SPCSnapToNodeThreshold);
					np = dm.snapToNode(n, nt);
					if(np.capture) {
						if(!$D.isDefined(np.node.SPC)) {
							dm.mergeNodes(n, np.node);
							dm.set(["theNodes", np.nodeId, "SPC"], spc);
							dm.set(["theSPCs", spc.id, "node"], np.node);
							changed = true;
						} else {
							// dm.removeNode(n);
							// dm.removeSPC(spc);
							changed = false;
						}
					}
				}
				
				if (!S.snapToNode || (!(np && np.capture) && S.SPCSnapToLine)) {
					var nl = dm.snapToLine(n, lt);
					if (nl.capture){
						var nonnl = dm.splitLineElement(nl.line,nl.ratio);
						dm.mergeNodes(n,nonnl);
						dm.set(["theNodes", nonnl.id, "SPC"], spc);
						dm.set(["theSPCs", spc.id, "node"], nonnl);
						changed = true;
					}
				};

                if (S.snapToGrid) {
					var grid = S.grid;
					// dm.set(["theSPCs", spc.id], Math.round(spc.node.X / grid) * grid - n1.X, Math.round(n1.Y / grid) * grid - n1.Y); 
					dm.transitNode(["theNodes",spc.node.id], Math.round(spc.node.X / grid) * grid - spc.node.X, Math.round(spc.node.Y / grid) * grid - spc.node.Y); 
					$D.iterate(dm.theNodes,function(n){
						if (n.id != spc.node.id && $D.distance(n, spc.node) < 0.01) {
							n1 = dm.mergeNodes(spc.node, n);
						};
					});
					// dm.transitNode(n2.id, n2.X - Math.round(n2.X / grid) * grid, n2.Y - Math.round(n2.Y / grid) * grid); 
				};


				if(changed) {
					var msg = "add a single point constraint";
					this.logMessage(msg);
				} else {
					var msg = "the point is already fixed, abort";
				}
				this.afterUndoableCommand(true, changed, true);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			"drawCircle" : function(data) {
                // debugger;
                if (data.radius > 30) {
                    this.sketchSelect();
                    return;
                }

				this.beforeUndoableCommand();
				var discard;
				var dm = this.Domain;
				var S = this.settings;
				var cen = data.Centroid;
				var top;
				var d = S.circleSnapToSPCThreshold + 10;
				var theSPC;
				var changed = false;
				$D.iterate(dm.theSPCs, function(spc) {
					var d1 = $D.distance(spc.node, cen);
					var d2 = $D.distance(spc.getBottomCenter(), cen);
					var tmp = (d1 < d2) ? d1 : d2;
					if(tmp < d) {
						d = tmp;
						top = (d1 < d2) ? true : false;
						theSPC = spc;
					}
				});
				if(theSPC && d < S.circleSnapToSPCThreshold) {
					if(top) {
						if(theSPC.RZ == 1) {
							dm.set(["theSPCs", theSPC.id, "RZ"], 0);
						}
					} else {
						if(theSPC.direction === "up" || theSPC.direction === "down") {
							if(theSPC.X == 1) {
								dm.set(["theSPCs", theSPC.id, "X"], 0);
							}
						} else if(theSPC.direction === "left" || theSPC.direction === "right") {
							if(theSPC.Y == 1) {
								dm.set(["theSPCs", theSPC.id, "Y"], 0);
							}
						}
					}
					changed = true;
				}

				var msg;
				if(changed) {
					if(top) {
						msg = "make a pin";
					} else {
						msg = "make a roller";
					}
					this.logMessage(msg);
				} else {
					msg = "do nothing";
				}
				
				var touched = changed;
				this.afterUndoableCommand(touched, changed, true);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
			
			"loadLine" : function(data) {
				this.beforeUndoableCommand();
				var dm = this.Domain;
				var touched;
				var changed;
				var S = this.settings;
				var node;
				var freeEnd;
				var nodeAtArrowEnd;
				var dx, dy;
				var nt = S.loadSnapToNodeThreshold/S.viewPortScale;
				var lt = S.loadSnapToLineThreshold/S.viewPortScale;
				var np1 = dm.snapToNode(data.to, nt);
				var addOnNode = false;
				var uniformLoad = false;
				if (S.loadSnapToNode) {
					if (np1.capture) {
						node = np1.node;
						freeEnd = new $D.Node({
							X:data.from.X,
							Y:data.from.Y
						});
						
						nodeAtArrowEnd = true;
						dx = node.X - freeEnd.X;
						dy = node.Y - freeEnd.Y;
						addOnNode = true;

					} else {
						var np2 = dm.snapToNode(data.from, nt); 
						if (np2.capture) {
							node = np2.node;
							// freeEnd = data.to;
							freeEnd = new $D.Node({
								X:data.to.X,
								Y:data.to.Y
							});
							nodeAtArrowEnd = false;
							dx = freeEnd.X - node.X;
							dy = freeEnd.Y - node.Y;
							addOnNode = true;
						};
					};
				}
				
				
				if (!node && S.loadSnapToLine) {
					var nl = dm.snapToLine(data.to, lt);
					if (nl.capture){
						node = dm.splitLineElement(nl.line, nl.ratio, true);
						// freeEnd = data.from;
						freeEnd = new $D.Node({
							X:data.from.X,
							Y:data.from.Y
							// constraintDirection:{
								// X : nl.line.getDx(),
								// Y : nl.line.getDy()
							// },
							// constraintOnLine:new $D.LineElement({
								// nodes : [{
									// X : nl.line.getFrom().X - data.to.X + data.from.X,
									// Y : nl.line.getFrom().Y - data.to.Y + data.from.Y
								// },{
									// X : nl.line.getEnd().X - data.to.X + data.from.X,
									// Y : nl.line.getEnd().Y - data.to.Y + data.from.Y
								// }]
							// })
						});
						nodeAtArrowEnd = true;
						dx = node.X - freeEnd.X;
						dy = node.Y - freeEnd.Y;
					} else {
						nl = dm.snapToLine(data.from, lt);
						if (nl.capture) {
							node = dm.splitLineElement(nl.line, nl.ratio, true);
							// freeEnd = data.to;
							freeEnd = new $D.Node({
								X:data.to.X,
								Y:data.to.Y
								// constraintDirection:{
									// X : nl.line.getDx(),
									// Y : nl.line.getDy()
								// },
								// constraintOnLine:new $D.LineElement({
									// nodes : [{
										// X : nl.line.getFrom().X + data.to.X - data.from.X,
										// Y : nl.line.getFrom().Y + data.to.Y - data.from.Y
									// },{
										// X : nl.line.getEnd().X + data.to.X - data.from.X,
										// Y : nl.line.getEnd().Y + data.to.Y - data.from.Y
									// }]
								// })
							});
							nodeAtArrowEnd = false;
							dx = freeEnd.X - node.X;
							dy = freeEnd.Y - node.Y;
						}
					}
				};
				
				var load;
				if (node) {
					load = dm.createNodeLoad(node, freeEnd, nodeAtArrowEnd);
					// load = dm.createNodeLoad(node, freeEnd, nodeAtArrowEnd, dx, dy, 0.0);
					touched = true;
					changed = true;
					if (S.snapToGrid) {
						var grid = S.grid;
						
						if (load) {
							if (node) {
								dm.transitNode(["theNodes",node.id], Math.round(node.X / grid) * grid - node.X, Math.round(node.Y / grid) * grid - node.Y); 
								// dm.set(["theNodes",node.id,"X"],Math.round(node.X / grid) * grid)
								// dm.set(["theNodes",node.id, "Y"],Math.round(node.Y / grid) * grid)
								node.X = dm.theNodes[node.id].X;
								node.Y = dm.theNodes[node.id].Y;
								$D.iterate(dm.theNodes,function(n){
									if (n.id != node.id && $D.distance(n,node) < 0.01) {
										node = dm.mergeNodes(node, n);
									};
								});
							} 
							if (freeEnd) {
								dm.transitNode(["currentPattern","Loads",load.id,"freeEnd"], Math.round(freeEnd.X / grid) * grid - freeEnd.X, Math.round(freeEnd.Y / grid) * grid - freeEnd.Y); 
								freeEnd.X = load.freeEnd.X;
								freeEnd.Y = load.freeEnd.Y;
								// freeEnd.X = Math.round(freeEnd.X / grid) * grid;
								// freeEnd.Y = Math.round(freeEnd.Y / grid) * grid;
								// dm.set(["currentPattern","Loads",load.id,"freeEnd","X"],Math.round(freeEnd.X / grid) * grid)
								// dm.set(["currentPattern","Loads",load.id,"freeEnd","Y"],Math.round(freeEnd.Y / grid) * grid )
								$D.iterate(dm.theNodes,function(n){
									if (n.id != freeEnd.id && $D.distance(n,freeEnd) < 0.01) {
										freeEnd = dm.mergeNodes(freeEnd, n);
									};
								});
							} 
						}
						
					}
					if (freeEnd && !addOnNode) {
						dm.set(["currentPattern","Loads",load.id,"freeEnd","constraintOnLine"],new $D.LineElement({
							nodes : [{
								X : nl.line.getFrom().X + freeEnd.X - node.X,
								Y : nl.line.getFrom().Y + freeEnd.Y - node.Y
							},{
								X : nl.line.getEnd().X + freeEnd.X - node.X,
								Y : nl.line.getEnd().Y + freeEnd.Y - node.Y
							}]
						}));
					}
				} else {
					if (S.analysisType == 'Static') {

                        // var n1 = new $D.Node({
						// 	X:data.from.X,
						// 	Y:data.from.Y
						// });
						// var n2 = new $D.Node({
						// 	X:data.to.X,
						// 	Y:data.to.Y
						// });
						
						
						// load = dm.createUniformLoad(n1,n2);
						
						// touched = true;
						// changed = true;
                        
						$D.iterate(dm.theNodes,function(n){
							if (!$D.isDefined(n.SPC)) {
								var fE = new $D.Node({
									X:n.X + data.to.X - data.from.X,
									Y:n.Y + data.to.Y - data.from.Y
								});
								load = dm.createNodeLoad(n, fE, false);
								touched = true;
								changed = true;
					
							}
						});
					} else if (S.analysisType == 'Dynamic'){
						var n1 = new $D.Node({
							X:data.from.X,
							Y:data.from.Y
						});
						var n2 = new $D.Node({
							X:data.to.X,
							Y:data.to.Y
						});
						
						
						load = dm.createUniformLoad(n1,n2);
						
						touched = true;
						changed = true;
						
					} else {
						touched = false;
						changed = false;
						
					}
					uniformLoad = true;
					// touched = true;
					// changed = true;
					// S.dynamicsAnalysis = true;
					// S.analysisType = ''
					
					// S.autoAnalysis = false;
				}
				
				
				isAnalysisReadyToRun = true;
				var msg;
				if(changed) {
					if (uniformLoad) {
						msg = "add uniform loads";
					} else {
						msg = "add a point load";
					}
				} else {
					msg = "do nothing";
				}
				this.afterUndoableCommand(touched, changed, msg);
				this.printMessage(msg);
				this.afterReanalyzeRequiredCommand();
				// this.refresh();
			},
            
			"loadSquareBracket" : function(data) {
				this.beforeUndoableCommand();
				var flag;

				if(flag) {
					var msg = "add a uniform distributed load";
				}
				this.afterUndoableCommand(flag, msg);
				// this.refresh();
			}
		},
		onPinchStart : function(e, el, obj) {
			var first = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			}), second = this.getCanvasCoordFromPage({
				X : e.touches[1].pageX,
				Y : e.touches[1].pageY
			});
			this.pinchCenter0 = {
				X : 0.5 * (first.X + second.X),
				Y : 0.5 * (first.Y + second.Y)
			};
            // this.pause = true;
            // this.settings.viewPortScaleSaved = this.settings.viewPortScale;
            this.lastScale = 1;
		},
		onPinch : function(e, el, obj) {
			if(!$D.isDefined(e.touches[0]) || !$D.isDefined(e.touches[1])) {
				return;
			}
            // this.viewPortScaleSaved = this.viewPortScale;
			var first = this.getCanvasCoordFromPage({
				X : e.touches[0].pageX,
				Y : e.touches[0].pageY
			}), second = this.getCanvasCoordFromPage({
				X : e.touches[1].pageX,
				Y : e.touches[1].pageY
			}), s = e.scale / this.lastScale, S = this.settings;
			this.pinchCenter1 = {
            				X : 0.5 * (first.X + second.X),
				Y : 0.5 * (first.Y + second.Y)
			};

            // var d = Math.sqrt((this.pinchCenter1.X - this.pinchCenter0.X) * (this.pinchCenter1.X - this.pinchCenter0.X)
            //                   + (this.pinchCenter1.Y - this.pinchCenter0.Y) * (this.pinchCenter1.Y - this.pinchCenter0.Y)) / S.viewPortScale;

            // this.printMessage('scale:' + s + ', shift: ' + d);
            // this.printMessage('v:' + d / Math.abs(s-1));
            // if (d / Math.abs(s-1) < 500) {
            if(S.viewPortScale * s < S.maxViewPortScale && S.viewPortScale * s > S.minViewPortScale) {
                S.viewPortShiftX = S.viewPortScale * this.pinchCenter1.X * (1 - s) + S.viewPortShiftX;
				S.viewPortShiftY = S.viewPortScale * this.pinchCenter1.Y * (1 - s) + S.viewPortShiftY;
				S.viewPortScale = S.viewPortScale * s;
                this.lastScale = e.scale;
                S.viewPortShiftX = this.pinchCenter1.X - this.pinchCenter0.X + S.viewPortShiftX;
                S.viewPortShiftY = this.pinchCenter1.Y - this.pinchCenter0.Y + S.viewPortShiftY;
				// S.viewPortScale = S.viewPortScaleSaved * s;
                // console.log("s:"+S.viewPortScale);
                // console.log("saved:"+S.viewPortScaleSaved);
                // console.log("viewPortScale:"+S.viewPortScale);
				// S.viewPortScale = S.viewPortScaleSaved * s;
				// this.deltaTransform(s, this.pinchCenter1.X - this.pinchCenter0.X * s, this.pinchCenter1.Y - this.pinchCenter0.Y * s);
				// this.recordDeltaTransform();
                
                
			} else if(S.viewPortScale * s >= S.maxViewPortScale) {
				alert("max scale reached");
			} else {
				alert("min scale reached");
			}
            // } else {
            //     S.viewPortShiftX = this.pinchCenter1.X - this.pinchCenter0.X + S.viewPortShiftX;
            //     S.viewPortShiftY = this.pinchCenter1.Y - this.pinchCenter0.Y + S.viewPortShiftY;
            // }
		},
		onMouseWheel : function(event) {
			// console.log("event",event)
			var e = event.browserEvent, //
			S = this.settings, //
			s = Math.pow(Math.E, e.wheelDelta * S.mouseWheelSpeedFactor / S.mouseWheelSpeedBase), //
			P = this.getCanvasCoordFromPage({
				X : e.pageX,
				Y : e.pageY
			});
            console.log('s:'+s);

			if(S.viewPortScale * s < S.maxViewPortScale && S.viewPortScale * s > S.minViewPortScale) {
				S.viewPortShiftX = S.viewPortScale * P.X * (1 - s) + S.viewPortShiftX;
				S.viewPortShiftY = S.viewPortScale * P.Y * (1 - s) + S.viewPortShiftY;
				S.viewPortScale = S.viewPortScale * s;
			} else if(S.viewPortScale * s >= S.maxViewPortScale) {
				alert("max scale reached");
			} else {
				alert("min scale reached");
			}

			// this.refresh();
		},
		onPinchEnd : function() {
			// this.recordDeltaTransform();
            // this.pause = false;
			// this.refresh();
		},
		autoScale: function() {
			//this.settings.autoDeformationScale = true;
			//this.settings.autoMomentScale = true;
			//this.analyze(function(){
				// this.refresh();
			//})
			sketchit.settings.deformationScale = sketchit.getAutoDeformationScale(sketchit.settings.maxDeformationOnScreen);
			
		},
		clearAll : function() {
			var r = confirm("Restart the sketch: you can not undo this operation, are you sure?");
			if(r === true) {
				// this.Domain.restart();
                delete this.Domain;
                this.Domain = new $D.Domain();
                window.Domain = this.Domain;
				this.views.undoButton.setDisabled(true);
				this.views.redoButton.setDisabled(true);
				// this.refresh();
			}
		},
		undo : function() {
			this.Domain.undo();
			this.views.redoButton.setDisabled(false);
			if(this.Domain._head === -1) {
				this.views.undoButton.setDisabled(true);
			}

			if(this.settings.autoAnalysis) {
				this.analyze(function() {
					// this.refresh();
				});
			} else {
				// this.refresh();
			}
			this.logMessage("undo");

		},
		redo : function() {
			this.Domain.redo();
			this.views.undoButton.setDisabled(false);
			if(this.Domain._head === this.Domain._timeline.length - 1) {
				this.views.redoButton.setDisabled(true);
			}

			if(this.settings.autoAnalysis) {
				this.analyze(function() {
					// this.refresh();
				});
			} else {
				// this.refresh();
			}
			this.logMessage("redo");
		},
		saveScript : function() {
			// alert(this.Domain.runStaticConstant(this.settings.modelScale, this.settings.loadScale));
			if (this.views.exportModelPanel.isHidden()) {
				this.views.exportModelPanel.show();
                if (this.settings.analysisType == 'Dynamic'){
				    this.views.exportModelPanel.getComponent(0).setValue(this.Domain.runDynamics());
                } else {
				    this.views.exportModelPanel.getComponent(0).setValue(this.Domain.runStaticConstant());
                }
				var J=this.Domain.exportToJSON();
				console.log(J);
				this.views.exportModelPanel.getComponent(1).setValue(JSON.stringify(J));
			} else {
				this.views.exportModelPanel.hide();
			}
		},
		showLog: function() {
			var str = "";
			str += "logs:\n";
			for (var i = 0; i < this.logs.length; i++) {
				str += this.logs[i] + "\n";
			}
			alert(str);
		},
		// sendCommand : function() {
// 			
		// },
		analyze : function() {
			var str = "";
			
			// if(this.Analysis.checkModel()) {
				// this.Analysis.analyze();
			// };
			
			if (this.settings.analysisType == 'Dynamic') {
				// if (this.Domain.checkModel()) {
// 						
				// }
// 				
				str = this.Domain.runDynamics(this.settings.dynamicAnalysisNsteps);
				// this.ws.send(this.Domain.runDynamics(this.settings.dynamicAnalysisNsteps));
				// this.ws.send("2]");
			} else if (this.settings.analysisType == 'Static') {
				str = this.Domain.runStaticConstant();
				// this.ws.send(this.Domain.runStaticConstant());
			}
			
			// avoid interupt inside a command
			var getSubstrIndex=function(str){
				var index = 4000;
				while(str[index]!=="\n" && str[index]!==";" && index>0) {
					index--;
				}
				console.log('; is at '+index);
				return index;
			};
			
			while(str.length>4000){
				var index = getSubstrIndex(str);
				// this.ws.send(str.slice(0,index+1));
				this.ws.emit('ops-stdin', str.slice(0,index+1));
				str = str.slice(index+1);
				
			}
			
			
			this.ws.emit('ops-stdin', str);
		},
		nodeSnapAnimate : function(nodesBefore, nodesAfter, fn) {
			var count = 0, dt = 1000 / this.settings.snapAnimationFps, dx = [], dy = [], it, i, len = nodesBefore.length;
			max = this.settings.snapAnimationElapse * this.settings.snapAnimationFps / 1000;
			for( i = 0; i < len; i++) {
				dx.push((nodesAfter[i].X - nodesBefore[i].X) / max);
				dy.push((nodesAfter[i].Y - nodesBefore[i].Y) / max);
			}
			it = setInterval(function(scope) {
				for( i = 0; i < len; i++) {
					scope.Domain.moveNode(nodesBefore[i].id, dx[i], dy[i]);
				}
				// scope.refresh()
				count++;
				if(count >= max) {
					clearInterval(it);
					fn.call(scope);
				}
			}, dt, this);
		}
	});
})();
