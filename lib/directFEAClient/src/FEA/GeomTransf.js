(function() {
	var $D=DirectFEA,GeomTransf;
	GeomTransf = $D.extend($D.Selectable, {
		defaults:{
			type:"PDelta" //["Linear","PDelta","Corotational"]
		},		
		toTcl:function(){
			return "geomTransf "+this.type+" "+this.id;
		}		
	});
	$D.GeomTransf=GeomTransf;
})();