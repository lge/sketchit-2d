(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var $D = DirectFEA, //
	Load = $D.extend($D.Selectable, {}), //
	PlainLoad = $D.extend(Load, {}), //
	UniformLoad = $D.extend(Load,{
		defaults : {
			ComponetName : "UniformLoad",
			type : "load",
			node : undefined,
			freeEnd : undefined,
			factor : undefined,
			
			// nodeAtArrowEnd : true,
			// arrowLength : undefined,
			// arrowAngle : undefined,

			X : undefined,
			Y : undefined,
			RZ : undefined,

			isSelected : false,
			isShow : true,
		},

		lineWidth : 10,
		normalStrokeStyle : [255, 125, 0, 255],
		selectedStrokeStyle : [255, 0, 255, 255],
		arrowHeadSize : 20,
		halfArrowAngle : Math.PI / 6,
		getDx : function() {
			return this.getEnd().X - this.getFrom().X;
		},
		getDy : function() {
			return this.getEnd().Y - this.getFrom().Y;
		},
		getFrom : function() {
			if (this.nodeAtArrowEnd) {
				return this.freeEnd;
			} else {
				return this.node;
			}
		},
		getEnd : function() {
			if (this.nodeAtArrowEnd) {
				return this.node;
			} else {
				return this.freeEnd;
			}
		},
		getAngle : function() {
			var a = Math.acos(this.getDx() / this.getLength());
			if(this.getDy() < 0) {
				a = -a;
			}
			return a
		},
		getLength : function() {
			return Math.sqrt((this.node.X - this.freeEnd.X) * (this.node.X - this.freeEnd.X) + (this.node.Y - this.freeEnd.Y) * (this.node.Y - this.freeEnd.Y));
		},

		display : function(R, scale) {
			var arrowLength = this.getLength();
			var arrowAngle = this.getAngle();
			if(!this.isShow) {
				return false;
			} else {
				R.lineWidth = this.lineWidth / scale;
				if(this.isSelected) {
					R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
				} else {
					R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
				}
				var start = this.node;

				R.save();
				R.drawArrow(start, arrowLength, arrowAngle, this.arrowHeadSize, this.halfArrowAngle);
				R.restore();
				
				if (this.freeEnd.isSelected === true) {
					this.freeEnd.display(R,scale);
				}
				return true;
			}
		},
		// toTcl : function() {
			// // return this.type + " " + this.node.id + " " + this.X + " " + this.Y + " " + this.RZ;
			// this.X = this.getEnd().X - this.getFrom().X;
			// this.Y = this.getEnd().Y - this.getFrom().Y;
			// this.RZ = 0.0;
			// return this.type + " " + this.node.id + " " + this.X + " " + this.Y + " " + this.RZ;
		// }
	}),
	NodeLoad = $D.extend(PlainLoad, {
		defaults : {
			ComponetName : "NodeLoad",
			type : "load",
			node : undefined,
			freeEnd : undefined,
			nodeAtArrowEnd : true,
			// arrowLength : undefined,
			// arrowAngle : undefined,

			X : undefined,
			Y : undefined,
			RZ : undefined,

			isSelected : false,
			isShow : true,
		},

		lineWidth : 3,
		normalStrokeStyle : [0, 0, 0, 255],
		selectedStrokeStyle : [255, 0, 255, 255],
		arrowHeadSize : 20,
		halfArrowAngle : Math.PI / 6,
		getDx : function() {
			return this.getEnd().X - this.getFrom().X;
		},
		getDy : function() {
			return this.getEnd().Y - this.getFrom().Y;
		},
		getFrom : function() {
			if (this.nodeAtArrowEnd) {
				return this.freeEnd;
			} else {
				return this.node;
			}
		},
		getEnd : function() {
			if (this.nodeAtArrowEnd) {
				return this.node;
			} else {
				return this.freeEnd;
			}
		},
		getAngle : function() {
			var a = Math.acos(this.getDx() / this.getLength());
			if(this.getDy() < 0) {
				a = -a;
			}
			return a
		},
		getLength : function() {
			return Math.sqrt((this.node.X - this.freeEnd.X) * (this.node.X - this.freeEnd.X) + (this.node.Y - this.freeEnd.Y) * (this.node.Y - this.freeEnd.Y));
		},

		display : function(R, scale) {
			var arrowLength = this.getLength();
			var arrowAngle = this.getAngle();
			if(!this.isShow) {
				return false;
			} else {
				R.lineWidth = this.lineWidth / scale;
				if(this.isSelected) {
					R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
				} else {
					R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
				}
				var start = this.nodeAtArrowEnd ? {
					X : this.node.X - arrowLength * Math.cos(arrowAngle),
					Y : this.node.Y - arrowLength * Math.sin(arrowAngle)
				} : this.node;

				R.save();
				R.drawArrow(start, arrowLength, arrowAngle, this.arrowHeadSize, this.halfArrowAngle);
				R.restore();
				
				if (this.freeEnd.isSelected === true) {
					this.freeEnd.display(R,scale);
				}
				return true;
			}
		},
		toTcl : function() {
			// return this.type + " " + this.node.id + " " + this.X + " " + this.Y + " " + this.RZ;
			this.X = this.getEnd().X - this.getFrom().X;
			this.Y = this.getEnd().Y - this.getFrom().Y;
			this.RZ = 0.0;
			return this.type + " " + this.node.id + " " + this.X + " " + this.Y + " " + this.RZ;
		}
	}), //
	PointElementLoad = $D.extend(PlainLoad, {
		lineWidth : 3,
		normalStrokeStyle : [0, 0, 0, 255],
		selectedStrokeStyle : [255, 0, 255, 255],
		arrowHeadSize : 20,
		halfArrowAngle : Math.PI / 6,

		defaults : {
			ComponetName : "PointElementLoad",
			element : undefined,
			Pz : undefined,
			xL : undefined,
			Px : undefined,

			elementAtArrowEnd : true,
			arrowLength : undefined,
			arrowAngle : undefined,

			isSelected : false,
			isShow : true,
		},
		toTcl : function() {
			return "eleLoad -ele " + this.element.id + " -type -beamPoint " + this.Pz + " " + this.xL + ($D.isDefined(this.Px) ? " " + this.Px : "");
		},
		getPointOnElememt : function() {
			return {
				X : this.element.getFrom().X + this.xL * (this.element.getEnd().X - this.element.getFrom().X),
				Y : this.element.getFrom().Y + this.xL * (this.element.getEnd().Y - this.element.getFrom().Y)
			}
		},
		display : function(R, scale) {
			if(!this.isShow) {
				return false;
			} else {
				R.lineWidth = this.lineWidth / scale;
				if(this.isSelected) {
					R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
				} else {
					R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
				}
				var node = this.getPointOnElememt(), start = this.elementAtArrowEnd ? {
					X : node.X - this.arrowLength * Math.cos(this.arrowAngle),
					Y : node.Y - this.arrowLength * Math.sin(this.arrowAngle)
				} : node;

				R.save();
				R.drawArrow(start, this.arrowLength, this.arrowAngle, this.arrowHeadSize, this.halfArrowAngle);
				R.restore();
				return true;
			}
		},
		//element:undefined
		//TODO:
		//X:
		//Y:
		//RZ:
		//toTcl:

	}), //
	UniformElementLoad = $D.extend(PlainLoad, {
		lineWidth : 1,
		normalFillStyle : [10, 245, 241, 127],
		selectedFillStyle : [10, 245, 241, 200],
		normalStrokeStyle : [0, 0, 0, 127],
		selectedStrokeStyle : [255, 0, 255, 127],
		arrowHeadSize : 8,
		halfArrowAngle : Math.PI / 8,
		arrowSpace : 10,

		defaults : {
			ComponetName : "UniformElementLoad",
			element : undefined,
			Wz : undefined,
			Wx : undefined,

			elementAtArrowEnd : true,
			arrowLength : undefined,

			isSelected : false,
			isShow : true,
		},
		toTcl : function() {
			return "eleLoad -ele " + this.element.id + " -type -beamUniform " + this.Wz + ($D.isDefined(this.Wx) ? " " + this.Wx : "");
		},
		display : function(R, scale) {
			if(!this.isShow) {
				return false;
			} else {
				var l, n, dx, dy, node, dangle, dArrowLenX, dArrowLenY, start, i;
				R.lineWidth = this.lineWidth / scale;
				if(this.isSelected) {
					R.strokeStyle = $D.array2rgba(this.selectedStrokeStyle);
					R.fillStyle = $D.array2rgba(this.selectedFillStyle);
				} else {
					R.strokeStyle = $D.array2rgba(this.normalStrokeStyle);
					R.fillStyle = $D.array2rgba(this.normalFillStyle);
				}
				l = this.element.getLength();
				n = Math.round(l / this.arrowSpace);
				dx = this.element.getDx() / n;
				dy = this.element.getDy() / n;
				node = {
					X : this.element.getFrom().X,
					Y : this.element.getFrom().Y
				};
				dangle = this.Wz > 0 ? Math.PI / 2 : -Math.PI / 2;
				dArrowLenX = this.arrowLength * Math.cos(this.element.getAngle() + dangle);
				dArrowLenY = this.arrowLength * Math.sin(this.element.getAngle() + dangle);

				R.drawFilledPolygon([{
					X : this.element.getFrom().X,
					Y : this.element.getFrom().Y
				}, {
					X : this.element.getFrom().X - (this.elementAtArrowEnd ? dArrowLenX : -dArrowLenX),
					Y : this.element.getFrom().Y - (this.elementAtArrowEnd ? dArrowLenY : -dArrowLenY),
				}, {
					X : this.element.getEnd().X - (this.elementAtArrowEnd ? dArrowLenX : -dArrowLenX),
					Y : this.element.getEnd().Y - (this.elementAtArrowEnd ? dArrowLenY : -dArrowLenY),
				}, {
					X : this.element.getEnd().X,
					Y : this.element.getEnd().Y
				}]);

				for( i = 0; i < n + 1; i++) {
					start = this.elementAtArrowEnd ? {
						X : node.X - dArrowLenX,
						Y : node.Y - dArrowLenY
					} : node;
					R.save();
					R.drawArrow(start, this.arrowLength, this.element.getAngle() + dangle, this.arrowHeadSize, this.halfArrowAngle);
					R.restore();
					node.X += dx;
					node.Y += dy;
				}
				return true;
			}
		},
		//element:undefined
		//TODO:
		//X:
		//Y:
		//RZ:
		//toTcl:

	});
	
	

	$D.Load = Load;
	$D.PlainLoad = PlainLoad;
	$D.UniformLoad = UniformLoad;
	$D.NodeLoad = NodeLoad;
	$D.PointElementLoad = PointElementLoad;
	$D.UniformElementLoad = UniformElementLoad;

})();
