(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var TimeSeries, ConstantTimeSeries,PathTimeSeries,//
	PlainPattern, UniformExcitationLoad, Pattern,PatternStore;
	$D = window.DirectFEA;
	
	Pattern = $D.extend($D.Selectable, {});
	
	PlainPattern=$D.extend(Pattern, {
		defaults:{
			className : "PlainPattern",
		},
		toTcl:function(){
			var result="";
			result+="pattern Plain "+this.id+" "+this.TimeSeries.id+" {;\n";
			$D.iterate(this.Loads,function(l){
                if (l.toTcl) {
				    result+=l.toTcl()+";\n";
                }
			})
			// this.Loads.each(function(l){
				// result+=l.toTcl()+";\n";
			// })
			//result+=this.Loads.eachtoTcl();
			result+="}"
			return result;	
		},
		// solve the circular reference problem:
		toJSON : function() {
			result={};
			
			for(i in this) {
				if(this.hasOwnProperty(i) && $D.isDefined(this[i]) && !$D.isObject(this[i]) && !$D.isArray(this[i])) {
					result[i]=this[i];
				}
			}
			
			var i;
			var loadIDs=[];
			for(i in this.nodes) {
				if(this.nodes.hasOwnProperty(i) && $D.isDefined(this.nodes[i])) {
					nodeIDs.push(i);
				}
			}
			result.nodeIDs = nodeIDs.length==0?"":nodeIDs;
			
			var i;
			var elementLoadsIDs=[];
			for(i in this.elementLoads) {
				if(this.elementLoads.hasOwnProperty(i) && $D.isDefined(this.elementLoads[i])) {
					elementLoadsIDs.push(i);
				}
			}
			result.elementLoadsIDs = elementLoadsIDs.length==0?"":elementLoadsIDs;
			
			result.geomTransfId=this.geomTransf?this.geomTransf.id:"";
			
			return result;
		}
		
		
		
	})
	
	UniformExcitationPattern=$D.extend(Pattern, {
		defaults:{
			className : "UniformExcitationPattern",
			direction:1
		},
		toTcl:function(){
			return "pattern UniformExcitation "+this.id+" "+this.direction+" -accel "+this.TimeSeries.id;			
		}
		
		
		
	})
	
	
	$D.Pattern = Pattern;
	$D.PlainPattern = PlainPattern;
	$D.UniformExcitationPattern = UniformExcitationPattern;

})();
