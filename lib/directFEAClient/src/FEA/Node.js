(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var $D = DirectFEA, Node;
	Node = $D.extend($D.Selectable, {
		className : "Node",
		defaults : {
			X : undefined,
			Y : undefined,
			beforeMoveX : undefined,
			beforeMoveY : undefined,
			dispX : 0.0,
			dispY : 0.0,
			dispRZ : 0.0,
			SPC : undefined,
			isSelected : false,
			isShow : true,
			showEdge : false,
			showDeformation : true,
			showTag:true,
			eleCount : 0,
			connectType : "beam", // "beam","truss",
			massX : 0.0,
			massY : 0.0,
			massRz : 0.0
			//nodeLoads : new $D.LoadStore()
		},
		constructor : function() {
			Node.superclass.constructor.apply(this, arguments);
			this.beforeMoveX = this.X;
			this.beforeMoveY = this.Y;
			
			
			this.nodeLoads = {};
			this.elements = {};
			this.ajacentNodes = {};
		},
		toTcl : function() {
			return "node " + this.id + " " + this.X + " " + this.Y + " -mass " + this.massX + " " + this.massY + " " + this.massRz;
		},
		nodeSize : 3,
		normalFillStyle : [255, 0, 0, 255],
		selectedFillStyle : [255, 0, 0, 255],
		edgeLineWidth : 2,
		edgeSize:5,
		edgeStrokeStyle : [150, 0, 255, 255],

		display : function(R, scale) {
			if(!this.isShow) {
				return false;
			} else {
				//console.log($D.array2rgba(this.selectedFillStyle));
				if(this.isSelected) {
					this.showEdge = true;
					R.fillStyle = $D.array2rgba(this.selectedFillStyle);
				} else {
					this.showEdge = false;
					R.fillStyle = $D.array2rgba(this.normalFillStyle);
				}
				R.drawSquare(this, this.nodeSize / scale, 1);
				if(this.showEdge === true) {
					R.lineWidth = this.edgeLineWidth / scale;
					R.strokeStyle = $D.array2rgba(this.edgeStrokeStyle);
					R.drawSquare(this, this.edgeSize, 0);
				}
				return true;
			}
		},
		tagShiftX:-10,
		tagShiftY:10,
		tagFont:"bold 12px sans-serif",
		tagFillStyle:[255, 0, 0, 127],
		
		displayTag : function(R, scale) {
			if(!this.showTag) {
				return false;
			} else {
				R.save();
				R.font = this.tagFont;
				R.fillStyle = $D.array2rgba(this.tagFillStyle);
				R.translate(this.X+this.tagShiftX/scale,this.Y+this.tagShiftY/scale)
				R.transform(1, 0, 0, -1, 0, 0);
				R.fillText(this.id, 0,0);
				R.restore();
				
				// R.fillStyle = $D.array2rgba(this.tagFillStyle);
				// R.drawSquare({
					// X : this.X + this.dispX * deformScale,
					// Y : this.Y + this.dispY * deformScale
				// }, this.deformedNodeSize / viewPortScale, 1);
				return true;
			}
		},
		deformedNodeSize : 3,
		deformedFillStyle : [0, 255, 0, 255],

		displayDeformation : function(R, viewPortScale, deformScale) {
			//console.log("df",arguments)
			if(!this.showDeformation) {
				return false;
			} else {
				R.fillStyle = $D.array2rgba(this.deformedFillStyle);
				R.drawSquare({
					X : this.X + this.dispX * deformScale,
					Y : this.Y + this.dispY * deformScale
				}, this.deformedNodeSize / viewPortScale, 1);
				return true;
			}

		},
		move : function(dx, dy) {
			// var isConstraint = $D.isDefined(constraint)? constraint: false;
			if ($D.isDefined(this.constraintOnLine) && this.enableConstraint === true) {
				var cl = this.constraintOnLine;
				var lamda;
				if (cl.getDx()) {
					lamda = (this.X - cl.getFrom().X) / cl.getDx();
				} else {
					lamda = (this.Y - cl.getFrom().Y) / cl.getDy();
				}
				var dlamda = (cl.getDx() * dx + cl.getDy() * dy) / (cl.getLength()*cl.getLength()); 
				if (lamda + dlamda <= 1 && lamda + dlamda >= 0) {
					lamda += dlamda;
				} else if (lamda + dlamda > 1) {
					lamda = 1;
				} else {
					lamda = 0;
				}
				this.X = cl.getFrom().X + lamda * cl.getDx();
				this.Y = cl.getFrom().Y + lamda * cl.getDy();
			} else if ($D.isDefined(this.constraintDirection)) {
				var dir = this.constraintDirection;
				var m = Math.sqrt(dir.X*dir.X+dir.Y*dir.Y);
				var dl = (dx * dir.X + dy * dir.Y) / m;
				this.X += dl*dir.X / m;
				this.Y += dl*dir.Y / m;
			} else {
				this.X += dx;
				this.Y += dy;
			}
			// console.log("dx,dy",dx,dy)
			// console.log("thisX,Y",this.X,this.Y)

		},
		getLocalId : function(elementID) {
			if(!$D.isDefined(this.elements[elementID])) {
				return -1;
			} else {
				for(var i = 0; i < this.elements[elementID].nodes.length; i++) {
					if(this.elements[elementID].nodes[i].id == this.id) {
						return i;
					}
				}
				return -1
			}
		},
		
		// solve the circular reference problem:
		toJSON : function() {
			result={};
			
			for(i in this) {
				if(this.hasOwnProperty(i) && $D.isDefined(this[i]) && !$D.isObject(this[i]) && !$D.isArray(this[i])) {
					result[i]=this[i];
				}
			}
			// result={
				// X : this.X,
				// Y : this.Y,
				// beforeMoveX : this.beforeMoveX,
				// beforeMoveY : this.beforeMoveY,
				// dispX : this.dispX,
				// dispY : this.dispY,
				// dispRZ : this.dispRZ,
				// isSelected : this.isSelected,
				// isShow : this.isShow,
				// showEdge : this.showEdge,
				// showDeformation : this.showDeformation,
				// showTag:this.showTag,
				// eleCount : this.eleCount,
				// connectType : this.connectType, // "beam","truss",
				// massX :this.massX,
				// massY : this.massY,
				// massRz :this.massRZ,
// 				
				// SPCID : (this.SPC?this.SPC.id:"")
			// };
			var i;
			var elementIDs=[];
			for(i in this.elements) {
				if(this.elements.hasOwnProperty(i) && $D.isDefined(this.elements[i])) {
					elementIDs.push(i);
				}
			}
			result.elementIDs = elementIDs.length==0?"":elementIDs;
			
			var i;
			var nodeLoadIDs=[];
			for(i in this.nodeLoads) {
				if(this.nodeLoads.hasOwnProperty(i) && $D.isDefined(this.nodeLoads[i])) {
					nodeLoadIDs.push(i);
				}
			}
			result.nodeLoadIDs = nodeLoadIDs.length==0?"":nodeLoadIDs;
			
			var i;
			var ajacentNodeIDs=[];
			for(i in this.ajacentNodes) {
				if(this.ajacentNodes.hasOwnProperty(i) && $D.isDefined(this.ajacentNodes[i])) {
					ajacentNodeIDs.push(i);
				}
			}
			result.ajacentNodeIDs = ajacentNodeIDs.length==0?"":ajacentNodeIDs;
			
			return result;
		},
		
		findReference:function(Dm){
			if (this.SPCID!=="") {
				this.SPC=Dm.theSPCs[this.SPCID];
			}
			if (this.elementIDs!=="") {
				for(var i=0,j=elementIDs.length; i<j; i++){
					this.elements[elementIDs[i]]=Dm.theElements[elementIDs[i]];
				}
			}
			if (this.ajacentNodeIDs!=="") {
				for(var i=0,j=ajacentNodeIDs.length; i<j; i++){
					this.ajacentNodes[ajacentNodeIDs[i]]=Dm.theNodes[ajacentNodeIDs[i]];
				}
			}
		}
		// isAdjacent:function(nodeId){
// 			
// 			
		// },
		// getAdjacentNodes:function(){
			// for 
// 			
		// },
	});

	$D.Node = Node;

})();
