(function() {
	window.DirectFEA = window.DirectFEA ? window.DirectFEA : {};
	var $D = DirectFEA, Renderer, distance = $D.distance;
	Renderer2D = function(canvas) {
		var ctx = canvas.getContext("2d");
		$D.apply(ctx.__proto__, {
			drawLine : function(from, to) {
				this.beginPath();
				this.moveTo(from.X, from.Y);
				this.lineTo(to.X, to.Y);
				this.stroke();
				return this;
			},
			drawLines : function(vertices) {
				
			},
			drawLineStrip : function(vertice) {
				var i, len;
				len = vertice.length;
				if(len < 2) {
					return this;
				}
				this.beginPath();
				this.moveTo(vertice[0].X, vertice[0].Y);
				for( i = 1; i < len; i++) {
					this.lineTo(vertice[i].X, vertice[i].Y);
				}
				this.stroke();
				return this;
			},
			drawLineLoop : function(vertice) {
				var i, len;
				len = vertice.length;
				if(len < 3) {
					return this;
				}
				this.beginPath();
				this.moveTo(vertice[0].X, vertice[0].Y);
				for( i = 1; i < len; i++) {
					this.lineTo(vertice[i].X, vertice[i].Y);
				}
				this.closePath();
				this.stroke();
				return this;
			},
			//mode=  0:stroke, 1:filled, 2:stroke and filled
			drawSquare : function(center, halfEdgeLength, mode) {
				this.beginPath();
				this.moveTo(center.X - halfEdgeLength, center.Y - halfEdgeLength);
				this.lineTo(center.X + halfEdgeLength, center.Y - halfEdgeLength);
				this.lineTo(center.X + halfEdgeLength, center.Y + halfEdgeLength);
				this.lineTo(center.X - halfEdgeLength, center.Y + halfEdgeLength);
				this.closePath();
				if(mode == 0) {
					this.stroke();
				} else if(mode == 1) {
					this.fill();
				}
				return this;
			},
			drawGrid : function(gridx, gridy, left, right, bottom, top) {
				var l = Math.round(left / gridx) * gridx;
				this.beginPath();
				while(l < right) {
					this.moveTo(l, bottom);
					this.lineTo(l, top);
					l += gridx;
				}
				l = Math.round(bottom / gridy) * gridy;
				while(l < top) {
					this.moveTo(left, l);
					this.lineTo(right, l);
					l += gridy;
				}
				this.stroke();
				return this;
			},
			drawArrow : function(start, len, dir, arrowSize, halfArrowAngle) {
				this.translate(start.X, start.Y);
				this.rotate(dir);
				this.beginPath();
				this.moveTo(0, 0);
				this.lineTo(len, 0);
				this.moveTo(len - arrowSize * Math.cos(halfArrowAngle), arrowSize * Math.sin(halfArrowAngle));
				this.lineTo(len, 0);
				this.lineTo(len - arrowSize * Math.cos(halfArrowAngle), -arrowSize * Math.sin(halfArrowAngle));
				this.stroke();
				return this;
			},
			drawFilledPolygon : function(vertice) {
				var i, len;
				len = vertice.length;
				if(len < 3) {
					return this;
				}
				this.beginPath();
				this.moveTo(vertice[0].X, vertice[0].Y);
				for( i = 1; i < len; i++) {
					this.lineTo(vertice[i].X, vertice[i].Y);
				}
				this.closePath();
				this.fill();
				return this;

			},
			
		});
		console.log("ctx",ctx)
		window.ctx=ctx;
		return ctx;
	};
	$D.Renderer = Renderer2D;
})();
