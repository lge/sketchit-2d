module("Element", {
	setup : function() {
	}
});

test("method:getDeflection", function() {
	var $D = DirectFEA, n1 = new $D.Node({
		X : 100,
		Y : 100,
		dispX : 1.0,
		dispY : 1.0,
		dispRZ : 0.1,
	}), n2 = new $D.Node({
		X : 200,
		Y : 200,
		dispX : -1.0,
		dispY : -1.0,
		dispRZ : -0.1,
	}), ele = new $D.ElasticBeamColumn({
		// from : n1,
		// to : n2,
		nodes:[n1,n2]
	});
	// alert("d" + DirectFEA)
	console.log("end deflection:"+ele.getDeflection(1, 100, 3))
	ok(Math.abs(ele.getDeflection(0, 100, 1) - 0) < 0.00001);
	ok(Math.abs(ele.getDeflection(1, 100, 1) - 0) < 0.00001);
	ok(Math.abs(ele.getDeflection(0, 100, 3) - 0) < 0.00001);
	ok(Math.abs(ele.getDeflection(1, 100, 3) - 0) < 0.00001);
	

});
