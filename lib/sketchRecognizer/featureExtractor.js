function featureExtractor(){
	this.line=function(data){
		return {
			start : data.from,
			end : data.to,
			len : Math.sqrt((data.from.X-data.to.X)*(data.from.X-data.to.X)+(data.from.Y-data.to.Y)*(data.from.Y-data.to.Y)),
			dir : Math.atan2(data.to.Y-data.from.Y,data.to.X-data.from.X)
		}
	};
	
	this.arrow=function(data){
		var tail=data.from;
		var head;
		var d = -1;
		var p;
		var t=1;
		for (var i=1; i < data.ResamplePoints.length; i++) {
            // debugger; 
			p = data.ResamplePoints[i];
			b = Math.sqrt((p.X-tail.X)*(p.X-tail.X)+(p.Y-tail.Y)*(p.Y-tail.Y));
			if(b > d) {
				d = b;
				t = i;
			}
		};
		head = data.ResamplePoints[t];

        // Very dirty here:
        data.from = tail;
        data.to = head;
		return {
			start : tail,
			end : head,
			len : d,
			dir : Math.atan2(head.Y-tail.Y,head.X-tail.X)
		}
	};
	
	this.circle=function(data){
		var center = data.Centroid;
		return {
			center : center,
			radius : Math.sqrt((data.from.X-center.X)*(data.from.X-center.X)+(data.from.Y-center.Y)*(data.from.Y-center.Y))
		}
	};
	
	this.triangle=function(data){
		var center = data.Centroid;
		return {
			center : center,
			radius : Math.sqrt((data.from.X-center.X)*(data.from.X-center.X)+(data.from.Y-center.Y)*(data.from.Y-center.Y))
		}
	};
	
	this.squareBracket = function(data) {
		
	};
	
	
	
	
	
}

featureExtractor.prototype.extractFeature=function(gesture){
    if (isDefined(this[gesture.name])) {
	    gesture.features = this[gesture.name].call(this,gesture.data);
    } else {
        console.log("no more feature extracted");
    }
	return gesture;
}
