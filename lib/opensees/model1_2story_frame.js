model = {
	name : '2d-frame',
	ndm : 2,
	ndf : 3,
	nn : 6,
	ne : 6,
	nodes : null,
	elements : null 		
}

model.nodes=[
	{
		tag : 1,
		coor : [0.0,0.0],
		SPC : [1,1,1],
		MPC : null,
		mass: [0,0,0],
		load : [],
		disp : [],
		impm : [],		
	},{
		tag : 2,
		coor : [20.0,0.0],
		SPC : [1,1,1],
		MPC : null,
		mass: [0,0,0],
		load : [],
		disp : [],
		impm : [],		
	},{
		tag : 3,
		coor : [0.0,10.0],
		SPC : [0,0,0],
		MPC : null,
		mass: [0,0,0],
		load : [],
		disp : [],
		impm : [],		
	},{
		tag : 4,
		coor : [20.0,10.0],
		SPC : [0,0,0],
		MPC : null,
		mass: [0,0,0],
		load : [],
		disp : [],
		impm : [],		
	},{
		tag : 5,
		coor : [0.0,20.0],
		SPC : [0,0,0],
		MPC : null,
		mass: [0,0,0],
		load : [100,0,0],
		disp : [],
		impm : [],		
	},{
		tag : 6,
		coor : [20.0,20.0],
		SPC : [0,0,0],
		MPC : null,
		mass: [0,0,0],
		load : [],
		disp : [],
		impm : [],		
	}
]

